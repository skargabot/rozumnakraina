﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ViewModels.RK
{
    public class FacebookAuthModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
    }
}
