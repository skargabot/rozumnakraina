﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModel.ViewModels
{
    public class IncidentsStatsResult
    {
        public int Today { get; set; }
        public int TodayCritical { get; set; }
        public int All { get; set; }
        public int Closed { get; set; }
    }
}