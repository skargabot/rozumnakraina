﻿using DataModel.Enums;
using DataModel.Models.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ViewModels
{

    public class IncidentsConnectionViewModel
    {
        public int ConnectedIncidentId { get; set; }
        public IncidentsConnectionUnaryType ConnectionType { get; set; }
    }

    //public class ConnectedIncidentViewModel
    //{
    //    public int Id { get; set; }
    //    public double Latitude { get; set; }
    //    public double Longitude { get; set; }
    //    public string Description { get; set; }
    //    public DateTime Date { get; set; }

    //    public Enums.IncidentType Type { get; set; }
    //    public Enums.IncidentPriority Priority { get; set; }

    //    public List<AnalyticalIncidentPhoto> IncidentPhotos { get; set; }
    //}
}
