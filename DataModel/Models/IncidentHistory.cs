﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class IncidentHistoryEntity
    {
        public int Id { get; set; }
        public int IncidentId { get; set; }
        public string Event { get; set; }
        public DateTime Date { get; set; }
    }
}
