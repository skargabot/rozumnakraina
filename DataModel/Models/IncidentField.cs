﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Enums;

namespace DataModel.Models
{
    [Serializable]
    public class IncidentField
    {
        public int Id { get; set; }
        public IncidentFieldType Type { get; set; }
        public string Name { get; set; }
        public string Question { get; set; }
    }
}
