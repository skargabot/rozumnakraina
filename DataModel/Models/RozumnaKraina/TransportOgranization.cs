﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class TransportOgranization
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<DemoTransportRoute> Routes { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}
