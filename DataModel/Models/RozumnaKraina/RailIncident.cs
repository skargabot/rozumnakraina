﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class RailIncident : RKIncident
    {
        public int TrainId { get; set; }
        public virtual Train Train { get; set; }
        public int WagonNumber { get; set; }
        public virtual ICollection<RailIncidentType> RailIncidentTypes { get; set; }
    }
}
