﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class PostIncident : RKIncident
    {
        public string CityOrIndex { get; set; }
        public string Office { get; set; }
        public virtual ICollection<PostIncidentType> PostIncidentTypes { get; set; }
    }
}
