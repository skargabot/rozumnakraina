﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Enums;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoIncident
    {
        public int Id { get; set; }
        public DateTime CraetedAt { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string GeocodedAddress { get; set; }
        public int TransportTypeId { get; set; }
        public int TransportRouteId { get; set; }
        public int? TransportId { get; set; }
        public bool EnableNotifications { get; set; }
        public string Email { get; set; }
        public string AssignedUserId { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
        public string CustomIncidentType { get; set; }
        public IncidentStatePublic State { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public IncidentStatePublic? DeadlineState { get; set; }

        public ApplicationUser AssignedUser { get; set; }
        public ApplicationUser User { get; set; }
        public virtual DemoTransportType TransportType { get; set; }
        public virtual DemoTransportRoute TransportRoute { get; set; }
        public virtual DemoTransport Transport { get; set; }
        public ICollection<DemoIncidentTypeRelation> Relations { get; set; } = new List<DemoIncidentTypeRelation>();
        public ICollection<Image> Images { get; set; }
        public ICollection<IncidentMessage> Messages { get; set; }
    }
}
