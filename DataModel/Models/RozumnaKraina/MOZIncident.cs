﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class MozIncident : RKIncident
    {
        public int HospitalId { get; set; }
        public virtual Hospital Hospital { get; set; }
        public virtual ICollection<MozIncidentType> MozIncidentTypes { get; set; }
    }
}
