﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class Road
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RoadIncident> RoadIncidents { get; set; }
    }
}
