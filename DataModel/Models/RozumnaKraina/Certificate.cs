﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class Certificate
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int OrderId { get; set; }
        public string UserId { get; set; }
        public decimal CertificateSum { get; set; }

        public Order Order { get; set; }
        public ApplicationUser User { get; set; }

        public List<CertificateWork> CertificateWorks { get; set; }
        public List<CertificateItem> CertificateItems { get; set; }
    }
}
