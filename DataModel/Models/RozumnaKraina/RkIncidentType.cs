﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public enum RkIncidentType
    {
        Road,
        Moz,
        Post,
        Rail
    }
}
