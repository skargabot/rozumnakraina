﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class IncidentMessage
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; } = DateTime.Now;
        public string SenderId { get; set; }
        public int IncidentId { get; set; }

        public ICollection<Image> Images { get; set; }
        public DemoIncident Incident { get; set; }
        public ApplicationUser Sender { get; set; }
    }
}
