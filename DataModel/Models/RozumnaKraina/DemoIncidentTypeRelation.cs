﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoIncidentTypeRelation
    {
        public int IncidentId { get; set; }

        public int IncidentTypeId { get; set; }

        public virtual DemoIncident Incident { get; set; }

        public virtual DemoIncidentType IncidentType { get; set; }
    }
}
