﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class MozIncidentType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<MozIncident> MozIncidents { get; set; }
    }
}
