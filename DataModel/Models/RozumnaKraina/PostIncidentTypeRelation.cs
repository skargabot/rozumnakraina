﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class PostIncidentTypeRelation
    {
        [Key, Column(Order = 0)]
        public int PostIncidentId { get; set; }
        [Key, Column(Order = 1)]
        public int PostIncidentTypeId { get; set; }

        public virtual PostIncident PostIncident { get; set; }
        public virtual PostIncidentType PostIncidentType { get; set; }
    }
}
