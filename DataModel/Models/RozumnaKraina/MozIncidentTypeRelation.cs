﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class MozIncidentTypeRelation
    {
        [Key, Column(Order = 0)]
        public int MozIncidentId { get; set; }
        [Key, Column(Order = 1)]
        public int MozIncidentTypeId { get; set; }

        public virtual MozIncident MozIncident { get; set; }
        public virtual MozIncidentType MozIncidentType { get; set; }
    }
}
