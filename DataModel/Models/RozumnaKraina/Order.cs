﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Enums;

namespace DataModel.Models.RozumnaKraina
{
    public class Order
    {
        public int Id { get; set; }
        public string AutoBarnd { get; set; }
        public string AutoMark { get; set; }
        public int AutoYear { get; set; }
        public DateTime WorkStart { get; set; }
        public DateTime WorkEnd { get; set; }
        public int IncidentId { get; set; }
        public string AssignedUserId { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal OrderSum { get; set; }
        public OrderState State { get; set; }
        public string WorkPlan { get; set; }

        public DemoIncident Incident { get; set; }
        public ApplicationUser AssignedUser { get; set; }
        public ApplicationUser User { get; set; }

        public List<OrderWork> OrderWorks { get; set; }
        public List<OrderItem> OrderItems { get; set; }
    }
}
