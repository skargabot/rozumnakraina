﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class RailIncidentTypeRelation
    {
        [Key, Column(Order = 0)]
        public int RailIncidentId { get; set; }
        [Key, Column(Order = 1)]
        public int RailIncidentTypeId { get; set; }

        public virtual RailIncident RailIncident { get; set; }
        public virtual RailIncidentType RailIncidentType { get; set; }
    }
}
