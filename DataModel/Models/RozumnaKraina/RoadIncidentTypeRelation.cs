﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class RoadIncidentTypeRelation
    {
        [Key, Column(Order = 0)]
        public int RoadIncidentId { get; set; }
        [Key, Column(Order = 1)]
        public int RoadIncidentTypeId { get; set; }

        public virtual RoadIncident RoadIncident { get; set; }
        public virtual RoadIncidentType RoadIncidentType { get; set; }
    }
}
