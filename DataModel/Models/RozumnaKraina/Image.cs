﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class Image
    {
        public string Id { get; set; }
        public string ContentType { get; set; }
        public int? RkIncidentId { get; set; }
        public int? IncidentMessageId { get; set; }

        public DemoIncident RkIncident { get; set; }
        public IncidentMessage IncidentMessage { get; set; }
    }
}
