﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class OrderItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public string Type { get; set; }
        public decimal Cost { get; set; }
        public int OrderId { get; set; }

        public Order Order { get; set; }
    }
}
