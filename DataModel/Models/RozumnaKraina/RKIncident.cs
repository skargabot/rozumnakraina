﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class RKIncident
    {
        public int Id { get; set; }
        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Image Image {get;set;}
        public DateTime Date { get; set; }
        public int OblastId { get; set; }
        public string Description { get; set; }
        public string City { get; set; }

        public RkIncidentType IncidentType { get; set; }

        [ForeignKey("OblastId")]
        public virtual Oblast Oblast { get; set; }
    }
}
