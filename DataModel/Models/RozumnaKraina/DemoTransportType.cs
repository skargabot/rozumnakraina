﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoTransportType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<DemoTransportRoute> Routes { get; set; } = new List<DemoTransportRoute>();

        public ICollection<DemoTransport> Transports { get; set; } = new List<DemoTransport>();
    }
}
