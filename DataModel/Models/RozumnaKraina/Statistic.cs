﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class Statistic
    {
        public int UsersCount { get; set; }
        public int ReportsCount { get; set; }
        public int UserReportsCount { get; set; }
        public List<StatisticItem> PopularRequests { get; set; }
        public List<StatisticItem> StatesTopReports { get; set; }
        public List<StatisticItem> RoadsTopReports { get; set; }

        public int Reports { get; set; }
        public int UserReports { get; set; }
    }
}
