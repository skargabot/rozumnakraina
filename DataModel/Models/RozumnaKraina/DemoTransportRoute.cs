﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoTransportRoute
    {
        public int Id { get; set; }

        public int TypeId { get; set; }

        public string Name { get; set; }

        public string StartStation { get; set; }

        public string StopStation { get; set; }

        public virtual DemoTransportType Type { get; set; }

        public int? TransportOgranizationId { get; set; }
        public TransportOgranization TransportOgranization { get; set; }

        public ICollection<DemoTransport> Transports { get; set; } = new List<DemoTransport>();
    }
}
