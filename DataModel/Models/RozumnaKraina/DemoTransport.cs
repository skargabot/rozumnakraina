﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoTransport
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public int? RouteId { get; set; }

        public int TypeId { get; set; }

        public virtual DemoTransportType Type { get; set; }

        public virtual DemoTransportRoute Route { get; set; }
    }
}
