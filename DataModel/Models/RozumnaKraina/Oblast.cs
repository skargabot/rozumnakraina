﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class Oblast
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<RKIncident> Incidents { get; set; }
    }
}
