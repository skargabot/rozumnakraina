﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class DemoIncidentType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool ForDrivers { get; set; }

        public ICollection<DemoIncidentTypeRelation> Relations { get; set; } = new List<DemoIncidentTypeRelation>();
    }
}
