﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.RozumnaKraina
{
    public class RoadIncident : RKIncident
    {
        public int Km { get; set; }
        public int RoadId { get; set; }
        public virtual Road Road { get; set; }
        public virtual ICollection<RoadIncidentType> RoadIncidentTypes { get; set; }

        public RoadIncident()
        {
            IncidentType = RkIncidentType.Road;
        }
    }
}
