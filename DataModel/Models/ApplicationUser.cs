﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataModel.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int? TransportOgranizationId { get; set; }
        public TransportOgranization TransportOgranization { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        [InverseProperty("AssignedUser")]
        public List<DemoIncident> AssignedIncidents { get; set; }
        [InverseProperty("User")]
        public List<DemoIncident> CreatedIncidents { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }
}
