﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class TransportRoute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class TransportRouteTypes
    {
        public string[] Value
        {
            get
            {
                return new string[] { "Автобус", "Маршрутка", "Трамвай", "Тролейбус" };
            }
        }
    }
}
