﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class TransportRouteType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}