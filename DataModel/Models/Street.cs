﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class Street
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string OldName { get; set; }
    }

    public class StreetTypes
    {
        public string[] Value
        {
            get
            {
                return new string[] { "Вулиця", "Проспект", "Площа", "Провулок", "Проїзд", "Тупик", "Узвіз" };
            }
        }
    }
}