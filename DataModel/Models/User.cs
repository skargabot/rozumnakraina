﻿using Newtonsoft.Json;

namespace DataModel.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        [JsonIgnore]
        public long? ChatId { get; set; }
        [JsonIgnore]
        public int? CreatingIncidentId { get; set; }
        /*public ICollection<Incident> Incidents { get; set; } */
    }
}
