﻿using System;

namespace DataModel.Models
{
    public class IncidentPhoto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }        
        public int IncidentId { get; set; }
        
        public Incident Incident { get; set; }
    }
}
