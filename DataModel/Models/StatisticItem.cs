﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class StatisticItem
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}
