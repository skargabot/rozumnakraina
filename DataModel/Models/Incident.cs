﻿using System;
using System.Collections.Generic;
using DataModel.Enums;
using System.ComponentModel;

namespace DataModel.Models
{
    public class Incident
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public IncidentStatePublic State { get; set; }
        public IncidentStateInternal StateInternal { get; set; }
        public IncidentType Type { get; set; }
        public IncidentPriority Priority { get; set; }

        public ICollection<IncidentPhoto> IncidentPhotos { get; set; }
        public ICollection<IncidentHistoryEntity> IncidentHistory { get; set; }
        public ICollection<IncidentMention> IncidentMentions { get; set; }

        public int? StreetId { get; set; }
        public Street Street { get; set; }

        public bool IsComplain { get; set; }

        public int? TransportRouteId { get; set; }
        public TransportRoute TransportRoute { get; set; }

        public ICollection<IncidentFieldValue> IncidentFieldsValues { get; set; }
    }
    public enum IncidentType
    {
        [Description("Стан доріг")]
        Road,
        [Description("Громадський транспорт")]
        PublicTransportation
    }
}
