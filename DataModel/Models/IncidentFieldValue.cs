﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class IncidentFieldValue
    {
        public int Id { get; set; }
        public int IncidentId { get; set; }
        public int IncidentFieldId { get; set; }
        public string Value { get; set; }
    }
}
