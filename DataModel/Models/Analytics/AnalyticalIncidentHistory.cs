﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.Analytics
{
    public class AnalyticalIncidentHistoryEntity
    {
        public int Id { get; set; }
        public int AnalyticalIncidentId { get; set; }
        public string Event { get; set; }
        public DateTime Date { get; set; }
    }
}
