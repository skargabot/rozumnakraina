﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel.Models.Analytics
{
    public class IncidentsConnection
    {
        [Key, Column(Order = 0)]
        public int Incident1Id { get; set; }
        [Key, Column(Order = 1)]
        public int Incident2Id { get; set; }
        public int ConnectionTypeId { get; set; }
    }
}
