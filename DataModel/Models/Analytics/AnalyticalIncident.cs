﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.Analytics
{
    public class AnalyticalIncident
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }

        //public Enums.IncidentType Type { get; set; }
        public Enums.IncidentPriority Priority { get; set; }

        public ICollection<AnalyticalIncidentPhoto> IncidentPhotos { get; set; }

        public ICollection<IncidentsConnection> IncidentsConnections { get; set; }

        public ICollection<AnalyticalIncidentHistoryEntity> IncidentHistory { get; set; }
    }
}
