﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class IncidentFieldValueSet
    {
        public int Id { get; set; }
        public int IncidentFieldId { get; set; }
        public string Value { get; set; }
    }
}
