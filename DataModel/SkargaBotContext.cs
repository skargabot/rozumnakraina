﻿using System.Data.Entity;
using DataModel.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using DataModel.Models.Analytics;
using DataModel.Models.RozumnaKraina;

namespace DataModel
{
    public class SkargaBotContext : IdentityDbContext<IdentityUser>
    {
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<IncidentPhoto> IncidentPhotos { get; set; }
        public DbSet<IncidentMention> IncidentMentions { get; set; }
        public DbSet<IncidentField> IncidentFields { get; set; }
        public DbSet<IncidentFieldValue> IncidentFieldValues { get; set; }
        public DbSet<IncidentFieldValueSet> IncidentFieldValueSets { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<TransportRoute> TransportRoutes { get; set; }
        public DbSet<Models.RozumnaKraina.RKIncident> RKIncidents { get; set; }
        public DbSet<Models.RozumnaKraina.Oblast> RKOblasts { get; set; }
        public DbSet<Models.RozumnaKraina.Road> RKRoads { get; set; }
        public DbSet<Models.RozumnaKraina.RoadIncidentType> RoadIncidentTypes { get; set; }
        public DbSet<Models.RozumnaKraina.RoadIncidentTypeRelation> RoadIncidentTypeRelations { get; set; }
        public DbSet<Models.RozumnaKraina.Image> Images { get; set; }
        public DbSet<Models.RozumnaKraina.PostIncidentType> PostIncidentTypes { get; set; }
        public DbSet<Models.RozumnaKraina.PostIncidentTypeRelation> PostIncidentTypeRelations { get; set; }
        public DbSet<Models.RozumnaKraina.RailIncidentType> RailIncidentTypes { get; set; }
        public DbSet<Models.RozumnaKraina.RailIncidentTypeRelation> RailIncidentTypeRelations { get; set; }
        public DbSet<Models.RozumnaKraina.Train> RKTrains { get; set; }
        public DbSet<Models.RozumnaKraina.MozIncidentType> MozIncidentTypes { get; set; }
        public DbSet<Models.RozumnaKraina.MozIncidentTypeRelation> MozIncidentTypeRelations { get; set; }
        public DbSet<Models.RozumnaKraina.Hospital> RKHospitals { get; set; }

        public DbSet<Models.RozumnaKraina.DemoIncident> RKDIncidents { get; set; }
        public DbSet<Models.RozumnaKraina.DemoIncidentType> RKDIncidentTypes { get; set; }
        public DbSet<Models.RozumnaKraina.DemoIncidentTypeRelation> RKDIncidentTypeRelations { get; set; }
        public DbSet<Models.RozumnaKraina.DemoTransport> RKDTransports { get; set; }
        public DbSet<Models.RozumnaKraina.DemoTransportType> RKDTransportTypes { get; set; }
        public DbSet<Models.RozumnaKraina.DemoTransportRoute> RKDTransportRoutes { get; set; }
        public DbSet<TransportOgranization> TransportOgranizations { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderWork> OrderWorks { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<CertificateItem> CertificateItems { get; set; }
        public DbSet<CertificateWork> CertificateWorks { get; set; }
        public DbSet<IncidentMessage> IncidentMessages { get; set; }

        public SkargaBotContext() : this("SkargaBotConnectionString")
        {
            
        }

        public SkargaBotContext(string connString)
            : base(connString)
        {
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public static SkargaBotContext Create()
        {
            return new SkargaBotContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Models.RozumnaKraina.DemoIncident>().HasKey(x => x.Id);
            modelBuilder.Entity<Models.RozumnaKraina.DemoIncidentType>().HasKey(x => x.Id);
            modelBuilder.Entity<Models.RozumnaKraina.DemoTransport>().HasKey(x => x.Id);
            modelBuilder.Entity<Models.RozumnaKraina.DemoTransportType>().HasKey(x => x.Id);
            modelBuilder.Entity<Models.RozumnaKraina.DemoTransportRoute>().HasKey(x => x.Id);

            modelBuilder.Entity<Models.RozumnaKraina.DemoIncidentTypeRelation>().HasKey(x => new { x.IncidentId, x.IncidentTypeId });
        }
    }
}
