﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Enums
{
    public enum IncidentStatePublic
    {
        [Display(Name = "Новий")]
        New,
        [Display(Name = "Прочитано")]
        Read,
        [Display(Name = "Передано в роботу")]
        InWork,
        [Display(Name = "Роботи закінчено")]
        WorkDone,
        [Display(Name = "Роботи призупинено")]
        WorkStopped,
        [Display(Name = "Необхідна перевірка")]
        CheckNeeded,
        [Display(Name = "Виконано")]
        Closed,
        [Display(Name = "Відкрито повторно")]
        Reopened
    }
}
