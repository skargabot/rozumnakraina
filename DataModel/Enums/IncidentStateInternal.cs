﻿namespace DataModel.Enums
{
    public enum IncidentStateInternal
    {
        New,
        Assigned,
        InProgress,
        Completed,
        Revised,
        Reopened,
        Postponed,
        Delivered,
        Closed,
        Deleted
    }
}
