﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Enums
{
    public enum OrderState
    {
        [Display(Name = "Новий")]
        New,
        [Display(Name = "В роботi")]
        InWork,
        [Display(Name = "Виконано")]
        Closed,
    }
}
