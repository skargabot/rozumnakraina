﻿namespace DataModel.Enums
{
    public enum CreateReportStepsEnum
    {
        CreationApproveWithDescription,
        CreationApproveWithPhoto,
        PhotoQuestion,
        EnterPhoto,
        EnterDescription, 
        GetLocation,
        GetType,
        Created,
        Canceled
    }
}
