namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncidentState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemoIncidents", "State");
        }
    }
}
