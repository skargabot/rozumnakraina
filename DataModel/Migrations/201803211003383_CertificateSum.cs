namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CertificateSum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Certificates", "CertificateSum", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Certificates", "CertificateSum");
        }
    }
}
