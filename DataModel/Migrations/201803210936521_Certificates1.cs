namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Certificates1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Certificates", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Certificates", "UserId");
            AddForeignKey("dbo.Certificates", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Certificates", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Certificates", new[] { "UserId" });
            DropColumn("dbo.Certificates", "UserId");
        }
    }
}
