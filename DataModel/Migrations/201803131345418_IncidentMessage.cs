namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncidentMessage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IncidentMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        PostDate = c.DateTime(nullable: false),
                        SenderId = c.String(maxLength: 128),
                        IncidentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DemoIncidents", t => t.IncidentId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.IncidentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IncidentMessages", "SenderId", "dbo.AspNetUsers");
            DropForeignKey("dbo.IncidentMessages", "IncidentId", "dbo.DemoIncidents");
            DropIndex("dbo.IncidentMessages", new[] { "IncidentId" });
            DropIndex("dbo.IncidentMessages", new[] { "SenderId" });
            DropTable("dbo.IncidentMessages");
        }
    }
}
