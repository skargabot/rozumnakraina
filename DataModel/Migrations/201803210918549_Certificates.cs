namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Certificates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CertificateItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Count = c.Int(nullable: false),
                        Type = c.String(),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CertificateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Certificates", t => t.CertificateId, cascadeDelete: true)
                .Index(t => t.CertificateId);
            
            CreateTable(
                "dbo.Certificates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.CertificateWorks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Count = c.Int(nullable: false),
                        Time = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CertificateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Certificates", t => t.CertificateId, cascadeDelete: true)
                .Index(t => t.CertificateId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CertificateWorks", "CertificateId", "dbo.Certificates");
            DropForeignKey("dbo.CertificateItems", "CertificateId", "dbo.Certificates");
            DropForeignKey("dbo.Certificates", "OrderId", "dbo.Orders");
            DropIndex("dbo.CertificateWorks", new[] { "CertificateId" });
            DropIndex("dbo.Certificates", new[] { "OrderId" });
            DropIndex("dbo.CertificateItems", new[] { "CertificateId" });
            DropTable("dbo.CertificateWorks");
            DropTable("dbo.Certificates");
            DropTable("dbo.CertificateItems");
        }
    }
}
