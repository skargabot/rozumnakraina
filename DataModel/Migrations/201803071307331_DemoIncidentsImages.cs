namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoIncidentsImages : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM dbo.Images");
            AlterColumn("dbo.Images", "RkIncidentId", c => c.Int());
            CreateIndex("dbo.Images", "RkIncidentId");
            AddForeignKey("dbo.Images", "RkIncidentId", "dbo.DemoIncidents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "RkIncidentId", "dbo.DemoIncidents");
            DropIndex("dbo.Images", new[] { "RkIncidentId" });
            AlterColumn("dbo.Images", "RkIncidentId", c => c.Int(nullable: false));
        }
    }
}
