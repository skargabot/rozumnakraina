namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Roads : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RKIncidents", "IncidentType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RKIncidents", "IncidentType");
        }
    }
}
