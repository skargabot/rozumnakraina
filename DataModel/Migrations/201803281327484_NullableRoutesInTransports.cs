namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableRoutesInTransports : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DemoTransports", "RouteId", "dbo.DemoTransportRoutes");
            DropIndex("dbo.DemoTransports", new[] { "RouteId" });
            AlterColumn("dbo.DemoTransports", "RouteId", c => c.Int());
            CreateIndex("dbo.DemoTransports", "RouteId");
            AddForeignKey("dbo.DemoTransports", "RouteId", "dbo.DemoTransportRoutes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DemoTransports", "RouteId", "dbo.DemoTransportRoutes");
            DropIndex("dbo.DemoTransports", new[] { "RouteId" });
            AlterColumn("dbo.DemoTransports", "RouteId", c => c.Int(nullable: false));
            CreateIndex("dbo.DemoTransports", "RouteId");
            AddForeignKey("dbo.DemoTransports", "RouteId", "dbo.DemoTransportRoutes", "Id", cascadeDelete: true);
        }
    }
}
