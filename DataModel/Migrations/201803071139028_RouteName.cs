namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RouteName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoTransportRoutes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemoTransportRoutes", "Name");
        }
    }
}
