namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomIncidentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "CustomIncidentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemoIncidents", "CustomIncidentType");
        }
    }
}
