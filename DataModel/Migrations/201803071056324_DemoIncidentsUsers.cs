namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemoIncidentsUsers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "AssignedUserId", c => c.String(maxLength: 128));
            AddColumn("dbo.DemoIncidents", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.DemoIncidents", "AssignedUserId");
            CreateIndex("dbo.DemoIncidents", "UserId");
            AddForeignKey("dbo.DemoIncidents", "AssignedUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.DemoIncidents", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DemoIncidents", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.DemoIncidents", "AssignedUserId", "dbo.AspNetUsers");
            DropIndex("dbo.DemoIncidents", new[] { "UserId" });
            DropIndex("dbo.DemoIncidents", new[] { "AssignedUserId" });
            DropColumn("dbo.DemoIncidents", "UserId");
            DropColumn("dbo.DemoIncidents", "AssignedUserId");
        }
    }
}
