namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        RKIncidentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncidentFields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Question = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncidentFieldValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidentId = c.Int(nullable: false),
                        IncidentFieldId = c.Int(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Incidents", t => t.IncidentId, cascadeDelete: true)
                .Index(t => t.IncidentId);
            
            CreateTable(
                "dbo.IncidentFieldValueSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidentFieldId = c.Int(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncidentMentions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidentId = c.Int(nullable: false),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Incidents", t => t.IncidentId, cascadeDelete: true)
                .Index(t => t.IncidentId);
            
            CreateTable(
                "dbo.IncidentPhotoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        ThumbnailUrl = c.String(),
                        IncidentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Incidents", t => t.IncidentId, cascadeDelete: true)
                .Index(t => t.IncidentId);
            
            CreateTable(
                "dbo.Incidents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Description = c.String(),
                        Date = c.DateTime(nullable: false),
                        UserId = c.Int(),
                        State = c.Int(nullable: false),
                        StateInternal = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        StreetId = c.Int(),
                        IsComplain = c.Boolean(nullable: false),
                        TransportRouteId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Streets", t => t.StreetId)
                .ForeignKey("dbo.TransportRoutes", t => t.TransportRouteId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.StreetId)
                .Index(t => t.TransportRouteId);
            
            CreateTable(
                "dbo.IncidentHistoryEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidentId = c.Int(nullable: false),
                        Event = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Incidents", t => t.IncidentId, cascadeDelete: true)
                .Index(t => t.IncidentId);
            
            CreateTable(
                "dbo.Streets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                        OldName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TransportRoutes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Phone = c.String(),
                        ChatId = c.Long(),
                        CreatingIncidentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MozIncidentTypeRelations",
                c => new
                    {
                        MozIncidentId = c.Int(nullable: false),
                        MozIncidentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MozIncidentId, t.MozIncidentTypeId })
                .ForeignKey("dbo.RKIncidents", t => t.MozIncidentId, cascadeDelete: true)
                .ForeignKey("dbo.MozIncidentTypes", t => t.MozIncidentTypeId, cascadeDelete: true)
                .Index(t => t.MozIncidentId)
                .Index(t => t.MozIncidentTypeId);
            
            CreateTable(
                "dbo.RKIncidents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(maxLength: 128),
                        Km = c.Int(nullable: false),
                        City = c.String(),
                        Date = c.DateTime(nullable: false),
                        RoadId = c.Int(nullable: false),
                        OblastId = c.Int(nullable: false),
                        Description = c.String(),
                        HospitalId = c.Int(),
                        CityOrIndex = c.String(),
                        Office = c.String(),
                        TrainId = c.Int(),
                        WagonNumber = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        RoadIncidentType_Id = c.Int(),
                        MozIncidentType_Id = c.Int(),
                        Image_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Hospitals", t => t.HospitalId, cascadeDelete: true)
                .ForeignKey("dbo.RoadIncidentTypes", t => t.RoadIncidentType_Id)
                .ForeignKey("dbo.MozIncidentTypes", t => t.MozIncidentType_Id)
                .ForeignKey("dbo.Trains", t => t.TrainId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .ForeignKey("dbo.Images", t => t.Image_Id)
                .ForeignKey("dbo.Oblasts", t => t.OblastId, cascadeDelete: true)
                .ForeignKey("dbo.Roads", t => t.RoadId, cascadeDelete: true)
                .Index(t => t.ApplicationUserID)
                .Index(t => t.RoadId)
                .Index(t => t.OblastId)
                .Index(t => t.HospitalId)
                .Index(t => t.TrainId)
                .Index(t => t.RoadIncidentType_Id)
                .Index(t => t.MozIncidentType_Id)
                .Index(t => t.Image_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Name = c.String(),
                        Surname = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Hospitals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Oblasts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoadIncidentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        RKIncident_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RKIncidents", t => t.RKIncident_Id)
                .Index(t => t.RKIncident_Id);
            
            CreateTable(
                "dbo.MozIncidentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostIncidentTypeRelations",
                c => new
                    {
                        PostIncidentId = c.Int(nullable: false),
                        PostIncidentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostIncidentId, t.PostIncidentTypeId })
                .ForeignKey("dbo.RKIncidents", t => t.PostIncidentId, cascadeDelete: true)
                .ForeignKey("dbo.PostIncidentTypes", t => t.PostIncidentTypeId, cascadeDelete: true)
                .Index(t => t.PostIncidentId)
                .Index(t => t.PostIncidentTypeId);
            
            CreateTable(
                "dbo.PostIncidentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RailIncidentTypeRelations",
                c => new
                    {
                        RailIncidentId = c.Int(nullable: false),
                        RailIncidentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RailIncidentId, t.RailIncidentTypeId })
                .ForeignKey("dbo.RKIncidents", t => t.RailIncidentId, cascadeDelete: true)
                .ForeignKey("dbo.RailIncidentTypes", t => t.RailIncidentTypeId, cascadeDelete: true)
                .Index(t => t.RailIncidentId)
                .Index(t => t.RailIncidentTypeId);
            
            CreateTable(
                "dbo.RailIncidentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Trains",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoadIncidentTypeRelations",
                c => new
                    {
                        RoadIncidentId = c.Int(nullable: false),
                        RoadIncidentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoadIncidentId, t.RoadIncidentTypeId })
                .ForeignKey("dbo.RKIncidents", t => t.RoadIncidentId, cascadeDelete: true)
                .ForeignKey("dbo.RoadIncidentTypes", t => t.RoadIncidentTypeId, cascadeDelete: true)
                .Index(t => t.RoadIncidentId)
                .Index(t => t.RoadIncidentTypeId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.PostIncidentTypePostIncidents",
                c => new
                    {
                        PostIncidentType_Id = c.Int(nullable: false),
                        PostIncident_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostIncidentType_Id, t.PostIncident_Id })
                .ForeignKey("dbo.PostIncidentTypes", t => t.PostIncidentType_Id, cascadeDelete: true)
                .ForeignKey("dbo.RKIncidents", t => t.PostIncident_Id, cascadeDelete: true)
                .Index(t => t.PostIncidentType_Id)
                .Index(t => t.PostIncident_Id);
            
            CreateTable(
                "dbo.RailIncidentTypeRailIncidents",
                c => new
                    {
                        RailIncidentType_Id = c.Int(nullable: false),
                        RailIncident_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RailIncidentType_Id, t.RailIncident_Id })
                .ForeignKey("dbo.RailIncidentTypes", t => t.RailIncidentType_Id, cascadeDelete: true)
                .ForeignKey("dbo.RKIncidents", t => t.RailIncident_Id, cascadeDelete: true)
                .Index(t => t.RailIncidentType_Id)
                .Index(t => t.RailIncident_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RoadIncidentTypeRelations", "RoadIncidentTypeId", "dbo.RoadIncidentTypes");
            DropForeignKey("dbo.RoadIncidentTypeRelations", "RoadIncidentId", "dbo.RKIncidents");
            DropForeignKey("dbo.RoadIncidentTypes", "RKIncident_Id", "dbo.RKIncidents");
            DropForeignKey("dbo.RKIncidents", "RoadId", "dbo.Roads");
            DropForeignKey("dbo.RKIncidents", "OblastId", "dbo.Oblasts");
            DropForeignKey("dbo.RKIncidents", "Image_Id", "dbo.Images");
            DropForeignKey("dbo.RKIncidents", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.RailIncidentTypeRelations", "RailIncidentTypeId", "dbo.RailIncidentTypes");
            DropForeignKey("dbo.RailIncidentTypeRelations", "RailIncidentId", "dbo.RKIncidents");
            DropForeignKey("dbo.RKIncidents", "TrainId", "dbo.Trains");
            DropForeignKey("dbo.RailIncidentTypeRailIncidents", "RailIncident_Id", "dbo.RKIncidents");
            DropForeignKey("dbo.RailIncidentTypeRailIncidents", "RailIncidentType_Id", "dbo.RailIncidentTypes");
            DropForeignKey("dbo.PostIncidentTypeRelations", "PostIncidentTypeId", "dbo.PostIncidentTypes");
            DropForeignKey("dbo.PostIncidentTypeRelations", "PostIncidentId", "dbo.RKIncidents");
            DropForeignKey("dbo.PostIncidentTypePostIncidents", "PostIncident_Id", "dbo.RKIncidents");
            DropForeignKey("dbo.PostIncidentTypePostIncidents", "PostIncidentType_Id", "dbo.PostIncidentTypes");
            DropForeignKey("dbo.MozIncidentTypeRelations", "MozIncidentTypeId", "dbo.MozIncidentTypes");
            DropForeignKey("dbo.RKIncidents", "MozIncidentType_Id", "dbo.MozIncidentTypes");
            DropForeignKey("dbo.MozIncidentTypeRelations", "MozIncidentId", "dbo.RKIncidents");
            DropForeignKey("dbo.RKIncidents", "RoadIncidentType_Id", "dbo.RoadIncidentTypes");
            DropForeignKey("dbo.RKIncidents", "HospitalId", "dbo.Hospitals");
            DropForeignKey("dbo.Incidents", "UserId", "dbo.Users");
            DropForeignKey("dbo.Incidents", "TransportRouteId", "dbo.TransportRoutes");
            DropForeignKey("dbo.Incidents", "StreetId", "dbo.Streets");
            DropForeignKey("dbo.IncidentPhotoes", "IncidentId", "dbo.Incidents");
            DropForeignKey("dbo.IncidentMentions", "IncidentId", "dbo.Incidents");
            DropForeignKey("dbo.IncidentHistoryEntities", "IncidentId", "dbo.Incidents");
            DropForeignKey("dbo.IncidentFieldValues", "IncidentId", "dbo.Incidents");
            DropIndex("dbo.RailIncidentTypeRailIncidents", new[] { "RailIncident_Id" });
            DropIndex("dbo.RailIncidentTypeRailIncidents", new[] { "RailIncidentType_Id" });
            DropIndex("dbo.PostIncidentTypePostIncidents", new[] { "PostIncident_Id" });
            DropIndex("dbo.PostIncidentTypePostIncidents", new[] { "PostIncidentType_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RoadIncidentTypeRelations", new[] { "RoadIncidentTypeId" });
            DropIndex("dbo.RoadIncidentTypeRelations", new[] { "RoadIncidentId" });
            DropIndex("dbo.RailIncidentTypeRelations", new[] { "RailIncidentTypeId" });
            DropIndex("dbo.RailIncidentTypeRelations", new[] { "RailIncidentId" });
            DropIndex("dbo.PostIncidentTypeRelations", new[] { "PostIncidentTypeId" });
            DropIndex("dbo.PostIncidentTypeRelations", new[] { "PostIncidentId" });
            DropIndex("dbo.RoadIncidentTypes", new[] { "RKIncident_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.RKIncidents", new[] { "Image_Id" });
            DropIndex("dbo.RKIncidents", new[] { "MozIncidentType_Id" });
            DropIndex("dbo.RKIncidents", new[] { "RoadIncidentType_Id" });
            DropIndex("dbo.RKIncidents", new[] { "TrainId" });
            DropIndex("dbo.RKIncidents", new[] { "HospitalId" });
            DropIndex("dbo.RKIncidents", new[] { "OblastId" });
            DropIndex("dbo.RKIncidents", new[] { "RoadId" });
            DropIndex("dbo.RKIncidents", new[] { "ApplicationUserID" });
            DropIndex("dbo.MozIncidentTypeRelations", new[] { "MozIncidentTypeId" });
            DropIndex("dbo.MozIncidentTypeRelations", new[] { "MozIncidentId" });
            DropIndex("dbo.IncidentHistoryEntities", new[] { "IncidentId" });
            DropIndex("dbo.Incidents", new[] { "TransportRouteId" });
            DropIndex("dbo.Incidents", new[] { "StreetId" });
            DropIndex("dbo.Incidents", new[] { "UserId" });
            DropIndex("dbo.IncidentPhotoes", new[] { "IncidentId" });
            DropIndex("dbo.IncidentMentions", new[] { "IncidentId" });
            DropIndex("dbo.IncidentFieldValues", new[] { "IncidentId" });
            DropTable("dbo.RailIncidentTypeRailIncidents");
            DropTable("dbo.PostIncidentTypePostIncidents");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RoadIncidentTypeRelations");
            DropTable("dbo.Trains");
            DropTable("dbo.RailIncidentTypes");
            DropTable("dbo.RailIncidentTypeRelations");
            DropTable("dbo.PostIncidentTypes");
            DropTable("dbo.PostIncidentTypeRelations");
            DropTable("dbo.MozIncidentTypes");
            DropTable("dbo.RoadIncidentTypes");
            DropTable("dbo.Roads");
            DropTable("dbo.Oblasts");
            DropTable("dbo.Hospitals");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.RKIncidents");
            DropTable("dbo.MozIncidentTypeRelations");
            DropTable("dbo.Users");
            DropTable("dbo.TransportRoutes");
            DropTable("dbo.Streets");
            DropTable("dbo.IncidentHistoryEntities");
            DropTable("dbo.Incidents");
            DropTable("dbo.IncidentPhotoes");
            DropTable("dbo.IncidentMentions");
            DropTable("dbo.IncidentFieldValueSets");
            DropTable("dbo.IncidentFieldValues");
            DropTable("dbo.IncidentFields");
            DropTable("dbo.Images");
        }
    }
}
