namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderSum", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Orders", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "State");
            DropColumn("dbo.Orders", "OrderSum");
        }
    }
}
