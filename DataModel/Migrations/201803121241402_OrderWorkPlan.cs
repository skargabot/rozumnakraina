namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderWorkPlan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "WorkPlan", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "WorkPlan");
        }
    }
}
