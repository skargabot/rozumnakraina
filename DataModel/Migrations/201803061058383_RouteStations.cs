namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RouteStations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoTransportRoutes", "StartStation", c => c.String());
            AddColumn("dbo.DemoTransportRoutes", "StopStation", c => c.String());
            DropColumn("dbo.DemoTransportRoutes", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DemoTransportRoutes", "Name", c => c.String());
            DropColumn("dbo.DemoTransportRoutes", "StopStation");
            DropColumn("dbo.DemoTransportRoutes", "StartStation");
        }
    }
}
