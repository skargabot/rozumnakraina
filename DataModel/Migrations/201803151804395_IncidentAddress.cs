namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncidentAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "GeocodedAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemoIncidents", "GeocodedAddress");
        }
    }
}
