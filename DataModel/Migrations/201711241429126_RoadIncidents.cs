namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoadIncidents : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RKIncidents", "RoadIncidentType_Id", "dbo.RoadIncidentTypes");
            DropForeignKey("dbo.RKIncidents", "MozIncidentType_Id", "dbo.MozIncidentTypes");
            DropForeignKey("dbo.RoadIncidentTypes", "RKIncident_Id", "dbo.RKIncidents");
            DropIndex("dbo.RKIncidents", new[] { "RoadId" });
            DropIndex("dbo.RKIncidents", new[] { "RoadIncidentType_Id" });
            DropIndex("dbo.RKIncidents", new[] { "MozIncidentType_Id" });
            DropIndex("dbo.RoadIncidentTypes", new[] { "RKIncident_Id" });
            CreateTable(
                "dbo.MozIncidentTypeMozIncidents",
                c => new
                    {
                        MozIncidentType_Id = c.Int(nullable: false),
                        MozIncident_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MozIncidentType_Id, t.MozIncident_Id })
                .ForeignKey("dbo.MozIncidentTypes", t => t.MozIncidentType_Id, cascadeDelete: true)
                .ForeignKey("dbo.RKIncidents", t => t.MozIncident_Id, cascadeDelete: true)
                .Index(t => t.MozIncidentType_Id)
                .Index(t => t.MozIncident_Id);
            
            CreateTable(
                "dbo.RoadIncidentTypeRoadIncidents",
                c => new
                    {
                        RoadIncidentType_Id = c.Int(nullable: false),
                        RoadIncident_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoadIncidentType_Id, t.RoadIncident_Id })
                .ForeignKey("dbo.RoadIncidentTypes", t => t.RoadIncidentType_Id, cascadeDelete: true)
                .ForeignKey("dbo.RKIncidents", t => t.RoadIncident_Id, cascadeDelete: true)
                .Index(t => t.RoadIncidentType_Id)
                .Index(t => t.RoadIncident_Id);
            
            AlterColumn("dbo.RKIncidents", "Km", c => c.Int());
            AlterColumn("dbo.RKIncidents", "RoadId", c => c.Int());
            CreateIndex("dbo.RKIncidents", "RoadId");
            DropColumn("dbo.RKIncidents", "RoadIncidentType_Id");
            DropColumn("dbo.RKIncidents", "MozIncidentType_Id");
            DropColumn("dbo.RoadIncidentTypes", "RKIncident_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RoadIncidentTypes", "RKIncident_Id", c => c.Int());
            AddColumn("dbo.RKIncidents", "MozIncidentType_Id", c => c.Int());
            AddColumn("dbo.RKIncidents", "RoadIncidentType_Id", c => c.Int());
            DropForeignKey("dbo.RoadIncidentTypeRoadIncidents", "RoadIncident_Id", "dbo.RKIncidents");
            DropForeignKey("dbo.RoadIncidentTypeRoadIncidents", "RoadIncidentType_Id", "dbo.RoadIncidentTypes");
            DropForeignKey("dbo.MozIncidentTypeMozIncidents", "MozIncident_Id", "dbo.RKIncidents");
            DropForeignKey("dbo.MozIncidentTypeMozIncidents", "MozIncidentType_Id", "dbo.MozIncidentTypes");
            DropIndex("dbo.RoadIncidentTypeRoadIncidents", new[] { "RoadIncident_Id" });
            DropIndex("dbo.RoadIncidentTypeRoadIncidents", new[] { "RoadIncidentType_Id" });
            DropIndex("dbo.MozIncidentTypeMozIncidents", new[] { "MozIncident_Id" });
            DropIndex("dbo.MozIncidentTypeMozIncidents", new[] { "MozIncidentType_Id" });
            DropIndex("dbo.RKIncidents", new[] { "RoadId" });
            AlterColumn("dbo.RKIncidents", "RoadId", c => c.Int(nullable: false));
            AlterColumn("dbo.RKIncidents", "Km", c => c.Int(nullable: false));
            DropTable("dbo.RoadIncidentTypeRoadIncidents");
            DropTable("dbo.MozIncidentTypeMozIncidents");
            CreateIndex("dbo.RoadIncidentTypes", "RKIncident_Id");
            CreateIndex("dbo.RKIncidents", "MozIncidentType_Id");
            CreateIndex("dbo.RKIncidents", "RoadIncidentType_Id");
            CreateIndex("dbo.RKIncidents", "RoadId");
            AddForeignKey("dbo.RoadIncidentTypes", "RKIncident_Id", "dbo.RKIncidents", "Id");
            AddForeignKey("dbo.RKIncidents", "MozIncidentType_Id", "dbo.MozIncidentTypes", "Id");
            AddForeignKey("dbo.RKIncidents", "RoadIncidentType_Id", "dbo.RoadIncidentTypes", "Id");
        }
    }
}
