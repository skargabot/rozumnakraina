namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImagesAndDeadline : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "DeadlineDate", c => c.DateTime());
            AddColumn("dbo.DemoIncidents", "DeadlineState", c => c.Int());
            AddColumn("dbo.Images", "IncidentMessageId", c => c.Int());
            CreateIndex("dbo.Images", "IncidentMessageId");
            AddForeignKey("dbo.Images", "IncidentMessageId", "dbo.IncidentMessages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "IncidentMessageId", "dbo.IncidentMessages");
            DropIndex("dbo.Images", new[] { "IncidentMessageId" });
            DropColumn("dbo.Images", "IncidentMessageId");
            DropColumn("dbo.DemoIncidents", "DeadlineState");
            DropColumn("dbo.DemoIncidents", "DeadlineDate");
        }
    }
}
