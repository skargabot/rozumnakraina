namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageContentFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Images", "ContentType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Images", "ContentType");
        }
    }
}
