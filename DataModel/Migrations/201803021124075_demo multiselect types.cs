namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class demomultiselecttypes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DemoIncidents", "TypeId", "dbo.DemoIncidentTypes");
            DropIndex("dbo.DemoIncidents", new[] { "TypeId" });
            RenameColumn(table: "dbo.DemoIncidents", name: "TypeId", newName: "DemoIncidentType_Id");
            CreateTable(
                "dbo.DemoIncidentTypeRelations",
                c => new
                    {
                        IncidentId = c.Int(nullable: false),
                        IncidentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IncidentId, t.IncidentTypeId })
                .ForeignKey("dbo.DemoIncidents", t => t.IncidentId, cascadeDelete: false)
                .ForeignKey("dbo.DemoIncidentTypes", t => t.IncidentTypeId, cascadeDelete: false)
                .Index(t => t.IncidentId)
                .Index(t => t.IncidentTypeId);
            
            AlterColumn("dbo.DemoIncidents", "DemoIncidentType_Id", c => c.Int());
            CreateIndex("dbo.DemoIncidents", "DemoIncidentType_Id");
            AddForeignKey("dbo.DemoIncidents", "DemoIncidentType_Id", "dbo.DemoIncidentTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DemoIncidents", "DemoIncidentType_Id", "dbo.DemoIncidentTypes");
            DropForeignKey("dbo.DemoIncidentTypeRelations", "IncidentTypeId", "dbo.DemoIncidentTypes");
            DropForeignKey("dbo.DemoIncidentTypeRelations", "IncidentId", "dbo.DemoIncidents");
            DropIndex("dbo.DemoIncidentTypeRelations", new[] { "IncidentTypeId" });
            DropIndex("dbo.DemoIncidentTypeRelations", new[] { "IncidentId" });
            DropIndex("dbo.DemoIncidents", new[] { "DemoIncidentType_Id" });
            AlterColumn("dbo.DemoIncidents", "DemoIncidentType_Id", c => c.Int(nullable: false));
            DropTable("dbo.DemoIncidentTypeRelations");
            RenameColumn(table: "dbo.DemoIncidents", name: "DemoIncidentType_Id", newName: "TypeId");
            CreateIndex("dbo.DemoIncidents", "TypeId");
            AddForeignKey("dbo.DemoIncidents", "TypeId", "dbo.DemoIncidentTypes", "Id", cascadeDelete: false);
        }
    }
}
