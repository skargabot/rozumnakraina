namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class demo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DemoIncidents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        CraetedAt = c.DateTime(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        TransportTypeId = c.Int(nullable: false),
                        TransportRouteId = c.Int(nullable: false),
                        TransportId = c.Int(),
                        EnableNotifications = c.Boolean(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DemoTransports", t => t.TransportId)
                .ForeignKey("dbo.DemoTransportRoutes", t => t.TransportRouteId, cascadeDelete: false)
                .ForeignKey("dbo.DemoTransportTypes", t => t.TransportTypeId, cascadeDelete: false)
                .ForeignKey("dbo.DemoIncidentTypes", t => t.TypeId, cascadeDelete: false)
                .Index(t => t.TypeId)
                .Index(t => t.TransportTypeId)
                .Index(t => t.TransportRouteId)
                .Index(t => t.TransportId);
            
            CreateTable(
                "dbo.DemoTransports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        RouteId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DemoTransportRoutes", t => t.RouteId, cascadeDelete: false)
                .ForeignKey("dbo.DemoTransportTypes", t => t.TypeId, cascadeDelete: false)
                .Index(t => t.RouteId)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.DemoTransportRoutes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DemoTransportTypes", t => t.TypeId, cascadeDelete: false)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.DemoTransportTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DemoIncidentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DemoIncidents", "TypeId", "dbo.DemoIncidentTypes");
            DropForeignKey("dbo.DemoIncidents", "TransportTypeId", "dbo.DemoTransportTypes");
            DropForeignKey("dbo.DemoIncidents", "TransportRouteId", "dbo.DemoTransportRoutes");
            DropForeignKey("dbo.DemoIncidents", "TransportId", "dbo.DemoTransports");
            DropForeignKey("dbo.DemoTransports", "TypeId", "dbo.DemoTransportTypes");
            DropForeignKey("dbo.DemoTransportRoutes", "TypeId", "dbo.DemoTransportTypes");
            DropForeignKey("dbo.DemoTransports", "RouteId", "dbo.DemoTransportRoutes");
            DropIndex("dbo.DemoTransportRoutes", new[] { "TypeId" });
            DropIndex("dbo.DemoTransports", new[] { "TypeId" });
            DropIndex("dbo.DemoTransports", new[] { "RouteId" });
            DropIndex("dbo.DemoIncidents", new[] { "TransportId" });
            DropIndex("dbo.DemoIncidents", new[] { "TransportRouteId" });
            DropIndex("dbo.DemoIncidents", new[] { "TransportTypeId" });
            DropIndex("dbo.DemoIncidents", new[] { "TypeId" });
            DropTable("dbo.DemoIncidentTypes");
            DropTable("dbo.DemoTransportTypes");
            DropTable("dbo.DemoTransportRoutes");
            DropTable("dbo.DemoTransports");
            DropTable("dbo.DemoIncidents");
        }
    }
}
