namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransportOrganizations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TransportOgranizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "TransportOgranizationId", c => c.Int());
            AddColumn("dbo.DemoTransportRoutes", "TransportOgranizationId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "TransportOgranizationId");
            CreateIndex("dbo.DemoTransportRoutes", "TransportOgranizationId");
            AddForeignKey("dbo.DemoTransportRoutes", "TransportOgranizationId", "dbo.TransportOgranizations", "Id");
            AddForeignKey("dbo.AspNetUsers", "TransportOgranizationId", "dbo.TransportOgranizations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "TransportOgranizationId", "dbo.TransportOgranizations");
            DropForeignKey("dbo.DemoTransportRoutes", "TransportOgranizationId", "dbo.TransportOgranizations");
            DropIndex("dbo.DemoTransportRoutes", new[] { "TransportOgranizationId" });
            DropIndex("dbo.AspNetUsers", new[] { "TransportOgranizationId" });
            DropColumn("dbo.DemoTransportRoutes", "TransportOgranizationId");
            DropColumn("dbo.AspNetUsers", "TransportOgranizationId");
            DropTable("dbo.TransportOgranizations");
        }
    }
}
