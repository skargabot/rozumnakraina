namespace DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class description : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemoIncidents", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemoIncidents", "Description");
        }
    }
}
