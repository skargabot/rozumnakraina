﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ApiModels.RK
{
    public class DemoIncidentDto
    {
        public DateTime CreatedAt { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? TransportId { get; set; }
        public int TransportTypeId { get; set; }
        public int TransportRouteId { get; set; }
        public int[] IncidentTypeIds { get; set; }
        public string Description { get; set; }
        public bool EnableNotifications { get; set; }
        public string Email { get; set; }
        public string CustomIncidentType { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
    }
}
