﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models.RozumnaKraina;

namespace DataModel.ApiModels.RK
{
    public class RoadIncident
    {
        public int RoadId { get; set; }
        public int OblastId { get; set; }
        public string City { get; set; }
        public int Km { get; set; }
        public DateTime Date { get; set; }
        public List<int> RoadIncidentTypes { get; set; }
        public string Description { get; set; }

        public Road Road { get; set; }
    }
}
