﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ApiModels.RK
{
    public class RailIncident
    {
        public int TrainId { get; set; }
        public int WagonNumber { get; set; }
        public DateTime Date { get; set; }
        public virtual ICollection<int> RailIncidentTypes { get; set; }
        public string Description { get; set; }
    }
}
