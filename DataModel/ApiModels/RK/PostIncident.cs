﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ApiModels.RK
{
    public class PostIncident 
    {
        public string CityOrIndex { get; set; }
        public string Office { get; set; }
        public virtual ICollection<int> PostIncidentTypes { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
