﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.ApiModels.RK
{
    public class MozIncident
    {
        public string City { get; set; }
        public DateTime Date { get; set; }
        public int HospitalId { get; set; }
        public int OblastId { get; set; }
        public virtual ICollection<int> RoadIncidentTypes { get; set; }
        public string Description { get; set; }
    }
}
