﻿using DataModel.Models.RozumnaKraina;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    public class RailIncidentRepository : BaseRepository
    {
        public int Create(ApiModels.RK.RailIncident incident, string userId)
        {
            var dataIncident = new Models.RozumnaKraina.RailIncident()
            {
                WagonNumber = incident.WagonNumber,
                Date = incident.Date,
                TrainId = incident.TrainId,
                Description = incident.Description,
                ApplicationUserID = userId
            };
            _ctx.RKIncidents.Add(dataIncident);
            _ctx.RailIncidentTypeRelations.AddRange(incident.RailIncidentTypes.Select(t => new RailIncidentTypeRelation
            {
                RailIncidentTypeId = t,
                RailIncident = dataIncident
            }));
            _ctx.SaveChanges();
            return dataIncident.Id;
        }
        public List<RailIncidentType> GetIncidentTypes()
        {
            return _ctx.RailIncidentTypes.ToList();
        }
        public List<Train> GetTrains()
        {
            return _ctx.RKTrains.ToList();
        }
    }
}
