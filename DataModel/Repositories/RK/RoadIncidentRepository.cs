﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models.RozumnaKraina;

namespace DataModel.Repositories.RK
{
    public class RoadIncidentRepository : BaseRepository
    {
        public int Create(ApiModels.RK.RoadIncident incident, string userId)
        {
            var dataIncident = new Models.RozumnaKraina.RoadIncident()
            {
                City = incident.City,
                Date = incident.Date,
                Km = incident.Km,
                OblastId = incident.OblastId,
                RoadId = incident.RoadId,
                Description = incident.Description,
                ApplicationUserID = userId,
                RoadIncidentTypes = _ctx.RoadIncidentTypes.Where(_ => incident.RoadIncidentTypes.Contains(_.Id)).ToList()
            };

            
            _ctx.RKIncidents.Add(dataIncident);
            //_ctx.RoadIncidentTypeRelations.AddRange(incident.RoadIncidentTypes.Select(t => new RoadIncidentTypeRelation
            //{
            //    RoadIncidentTypeId = t,
            //    RoadIncident = dataIncident
            //}));
            _ctx.SaveChanges();
            return dataIncident.Id;
        }
        public List<RoadIncidentType> GetIncidentTypes()
        {
            return _ctx.RoadIncidentTypes.Include(_ => _.RoadIncidents).ToList();
        }
        public List<Road> GetRoads()
        {
            return _ctx.RKRoads.Include(_ => _.RoadIncidents).ToList();
        }
        public List<Oblast> GetOblasts()
        {
            return _ctx.RKOblasts.Include(_ => _.Incidents).ToList();
        }
        public List<RoadIncident> GetIncidents()
        {
            return _ctx.RKIncidents.OfType<RoadIncident>().ToList();
        }
    }
}
