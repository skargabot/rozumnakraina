﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    public class ImportInfoRepository : BaseRepository
    {
        [DelimitedRecord("/")]
        public class OblastImport
        {
            public string Name { get; set; }
        }
        [DelimitedRecord("/")]
        public class RoadImport
        {
            public string Name { get; set; }
        }
        [DelimitedRecord("/")]
        public class RoadIncidentTypeImport
        {
            public string Name { get; set; }
        }
        [DelimitedRecord("/")]
        public class AbstractTypeImport
        {
            public string Name { get; set; }
        }
        public void ImportOblasts(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<OblastImport>();
            OblastImport[] oblastsImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RKOblasts.AddRange(oblastsImport.Select(s => new Models.RozumnaKraina.Oblast
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportRoads(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<OblastImport>();
            OblastImport[] roadsImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RKRoads.AddRange(roadsImport.Select(s => new Models.RozumnaKraina.Road
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportRoadIncidentTypes(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<RoadIncidentTypeImport>();
            RoadIncidentTypeImport[] roadIncidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RoadIncidentTypes.AddRange(roadIncidentTypesImport.Select(s => new Models.RozumnaKraina.RoadIncidentType
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportTrainIncidentTypes(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<AbstractTypeImport>();
            AbstractTypeImport[] incidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RailIncidentTypes.AddRange(incidentTypesImport.Select(s => new Models.RozumnaKraina.RailIncidentType
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportPostIncidentTypes(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<AbstractTypeImport>();
            AbstractTypeImport[] incidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.PostIncidentTypes.AddRange(incidentTypesImport.Select(s => new Models.RozumnaKraina.PostIncidentType
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportMozIncidentTypes(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<AbstractTypeImport>();
            AbstractTypeImport[] incidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.MozIncidentTypes.AddRange(incidentTypesImport.Select(s => new Models.RozumnaKraina.MozIncidentType
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportTrains(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<AbstractTypeImport>();
            AbstractTypeImport[] incidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RKTrains.AddRange(incidentTypesImport.Select(s => new Models.RozumnaKraina.Train
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportHospitals(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<AbstractTypeImport>();
            AbstractTypeImport[] incidentTypesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.RKHospitals.AddRange(incidentTypesImport.Select(s => new Models.RozumnaKraina.Hospital
            {
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
    }
}
