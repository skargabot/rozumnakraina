﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity.EntityFramework;
using DataModel.ViewModels.RK;

namespace DataModel.Repositories.RK
{
    public class AccountRepository : BaseRepository
    {
        private UserManager<ApplicationUser> _userManager;

        public AccountRepository()
        {
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));
           
        }

        public async Task<IdentityResult> RegisterUser(UserViewModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.Email,
                Surname = userModel.Surname,
                Name = userModel.Name,
                Email = userModel.Email
            };

            _userManager.UserValidator = new UserValidator<ApplicationUser>(_userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            var result = await _userManager.CreateAsync(user, userModel.Password);
            _userManager.AddToRole(user.Id, "Користувач");

            /*if (result.Succeeded)
            {
                var result1 = _userManager.AddToRoleAsync(user.Id, "RKUser");
            }*/
            return result;
        }
    }
}
