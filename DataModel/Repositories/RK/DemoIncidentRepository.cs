﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    using DataModel.ApiModels.RK;
    using DataModel.Models.RozumnaKraina;

    public class DemoIncidentRepository : BaseRepository
    {
        public IEnumerable<DemoIncident> GetIncidents()
        {
            return _ctx.RKDIncidents
                .Include(_ => _.Images)
                .Include(_ => _.Relations.Select(r => r.IncidentType))
                .Include(_ => _.TransportRoute.Type)
                .Include(_ => _.Transport)
                .Include(_ => _.Messages.Select(m => m.Sender))
                .ToList();
        }

        public string GetUserRole(string userId)
        {
            var role = _ctx.Roles.FirstOrDefault(_ =>
                _.Id == _ctx.Users.FirstOrDefault(r => r.Id == userId).Roles.FirstOrDefault().RoleId);
            return role?.Name;
        }

        public IEnumerable<DemoIncidentType> GetIncidentTypes()
        {
            return _ctx.RKDIncidentTypes.Include(_ => _.Relations.Select(r => r.Incident)).ToList();
        }

        public IEnumerable<DemoTransportType> GetTransportTypes()
        {
            return _ctx.RKDTransportTypes.ToList();
        }

        public IEnumerable<DemoTransportRoute> GetTransportRoutes()
        {
            return _ctx.RKDTransportRoutes.Include(_ => _.Type).ToList();
        }

        public IEnumerable<DemoTransport> GetTransports()
        {
            return _ctx.RKDTransports
                .Include(nameof(DemoTransport.Type))
                .Include(nameof(DemoTransport.Route))
                .ToList();
        }

        public async Task<DemoIncident> CreateIncident(DemoIncidentDto dto)
        {
            var dispatcher = _ctx.Users.Include(_ => _.Roles).FirstOrDefault(_ =>
                _.Roles.Any(r => r.RoleId == _ctx.Roles.FirstOrDefault(rr => rr.Name == "Адміністратор").Id));

            var incident = new DemoIncident
            {
                CraetedAt = dto.CreatedAt,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                TransportTypeId = dto.TransportTypeId,
                TransportRouteId = dto.TransportRouteId,
                TransportId = dto.TransportId,
                EnableNotifications = dto.EnableNotifications,
                Email = dto.Email,
                Description = dto.Description,
                UserId = dto.UserId,
                AssignedUserId = dispatcher?.Id,
                GeocodedAddress = dto.Address,
                CustomIncidentType = dto.CustomIncidentType
            };
            _ctx.RKDIncidents.Add(incident);

            foreach (var incidentTypeId in dto.IncidentTypeIds)
            {
                var incidentTypeRelation = new DemoIncidentTypeRelation
                {
                    Incident = incident,
                    IncidentTypeId = incidentTypeId
                };
                _ctx.RKDIncidentTypeRelations.Add(incidentTypeRelation);
            }
            
            _ctx.SaveChanges();

            return incident;
        }
    }
}
