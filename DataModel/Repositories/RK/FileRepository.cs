﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    public class FileRepository : BaseRepository
    {
        public void SaveFileInfo(int? incidentId, int? msgId, string fileId, string contentType)
        {
            _ctx.Images.Add(new Models.RozumnaKraina.Image
            {
                Id = fileId,
                RkIncidentId = incidentId,
                ContentType = contentType,
                IncidentMessageId = msgId
            });
            _ctx.SaveChanges();
        }
    }
}
