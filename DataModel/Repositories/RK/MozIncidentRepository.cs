﻿using DataModel.Models.RozumnaKraina;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    public class MozIncidentRepository : BaseRepository
    {
        public int Create(ApiModels.RK.MozIncident incident, string userId)
        {
            var dataIncident = new MozIncident()
            {
                City = incident.City,
                Date = incident.Date,
                OblastId = incident.OblastId,
                HospitalId = incident.HospitalId,
                Description = incident.Description,
                ApplicationUserID = userId
            };
            _ctx.RKIncidents.Add(dataIncident);
            _ctx.MozIncidentTypeRelations.AddRange(incident.RoadIncidentTypes.Select(t => new MozIncidentTypeRelation
            {
                MozIncidentTypeId = t,
                MozIncident = dataIncident
            }));
            _ctx.SaveChanges();
            return dataIncident.Id;
        }
        public List<MozIncidentType> GetIncidentTypes()
        {
            return _ctx.MozIncidentTypes.ToList();
        }
        public List<Hospital> GetHospitals()
        {
            return _ctx.RKHospitals.ToList();
        }
        public List<Oblast> GetOblasts()
        {
            return _ctx.RKOblasts.ToList();
        }
        public List<MozIncident> GetIncidents()
        {
            return _ctx.RKIncidents.OfType<MozIncident>().ToList();
        }
    }
}
