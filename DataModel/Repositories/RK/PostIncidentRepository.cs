﻿using DataModel.Models.RozumnaKraina;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories.RK
{
    public class PostIncidentRepository : BaseRepository
    {
        public int Create(ApiModels.RK.PostIncident incident, string userId)
        {
            var data = new PostIncident
            {
                CityOrIndex = incident.CityOrIndex,
                Date = incident.Date,
                Description = incident.Description,
                ApplicationUserID = userId,
                Office = incident.Office
            };
            _ctx.RKIncidents.Add(data);
            _ctx.PostIncidentTypeRelations.AddRange(incident.PostIncidentTypes.Select(t => new PostIncidentTypeRelation
            {
                PostIncidentTypeId = t,
                PostIncident = data
            }));
            _ctx.SaveChanges();
            return data.Id;
        }
        public List<PostIncidentType> GetIncidentTypes()
        {
            return _ctx.PostIncidentTypes.ToList();
        }
    }
}
