﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories
{
    public abstract class BaseRepository : IDisposable
    {
        protected SkargaBotContext _ctx;

        protected BaseRepository()
        {
            _ctx = new SkargaBotContext();
        }

        public int UsersCount()
        {
            return _ctx.Users.Count();
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
