﻿using DataModel.Models;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories
{
    public class ImportOfficialInfoRepository : BaseRepository
    {
        [DelimitedRecord(",")]
        public class StreetImport
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }
        [DelimitedRecord(",")]
        public class RouteImport
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }
        public void ImportStreets(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<StreetImport>();
            StreetImport[] streetsImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.Streets.AddRange(streetsImport.Select(s => new Street
            {
                Type = s.Type,
                Name = s.Name
            }));
            _ctx.SaveChanges();
        }
        public void ImportRoutes(System.IO.TextReader reader)
        {
            var engine = new FileHelperEngine<RouteImport>();
            RouteImport[] routesImport = engine.ReadStream(reader, int.MaxValue);

            _ctx.TransportRoutes.AddRange(routesImport.Select(s => new TransportRoute
            {
                Type = s.Type,
                Name = s.Name,
                
            }));
            _ctx.SaveChanges();
        }
    }
}
