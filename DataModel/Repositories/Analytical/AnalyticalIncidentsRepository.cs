﻿using DataModel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataModel.Models.Analytics;
using DataModel.Enums;

namespace DataModel.Repositories.Analytical
{
    /*public class AnalyticalIncidentsRepository : BaseRepository
    {
        public IEnumerable<AnalyticalIncidentViewModel> GetAnalyticalIncidentsViewModelsList()
        {
            return _ctx.AnalyticalIncidents.Include(t => t.IncidentPhotos).Select(i => Mapper.AnalyticalIncidentToViewModel(
                                i, _ctx.IncidentsConnections.Where(c => c.Incident1Id == i.Id || c.Incident2Id == i.Id).ToList())
                                );
        }

        public AnalyticalIncident FindAnalyticalIncident(int id)
        {
            return _ctx.AnalyticalIncidents.Find(id);
        }

        public void CreateIncidentsConnection(IncidentsConnection connection)
        {
            _ctx.IncidentsConnections.Add(connection);

            CreateIncidentHistoryEntity(connection.Incident1Id, string.Format("Інциндент зв'язано з інциндентом #{0}. Тип зв'язку: {1}",
                connection.Incident2Id, Mapper.IncidentsConnectionToUnary((IncidentsConnectionType)connection.ConnectionTypeId, true)));

            CreateIncidentHistoryEntity(connection.Incident2Id, string.Format("Інциндент зв'язано з інциндентом #{0}. Тип зв'язку: {1}",
                connection.Incident1Id, Mapper.IncidentsConnectionToUnary((IncidentsConnectionType)connection.ConnectionTypeId, false)));

            _ctx.SaveChanges();
        }

        public bool IncidentsConnectionExists(int id1, int id2)
        {
            return _ctx.IncidentsConnections.Any(e => (e.Incident1Id == id1 && e.Incident2Id == id2) || (e.Incident1Id == id2 && e.Incident2Id == id1));
        }

        public ICollection<AnalyticalIncidentHistoryEntity> GetAnalyticalIncidentHistory(AnalyticalIncident incident)
        {
            return incident.IncidentHistory;
        }

        protected void CreateIncidentHistoryEntity(int incidentId, string eventMessage)
        {
            _ctx.AnalyticalIncidentHistoryEntities.Add(new AnalyticalIncidentHistoryEntity
            {
                AnalyticalIncidentId = incidentId,
                Event = eventMessage,
                Date = DateTime.Now
            });
        }
    }*/
}
