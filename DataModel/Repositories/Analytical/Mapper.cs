﻿using DataModel.Models.Analytics;
using DataModel.Models;
using DataModel.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataModel.Enums;

namespace DataModel.Repositories.Analytical
{
    public class Mapper
    {
        /*public static AnalyticalIncident IncidentToAnalitical(Incident incident)
        {
            return new AnalyticalIncident
            {
                Id = incident.Id,
                Date = incident.Date,
                Description = incident.Description,
                Type = incident.Type,
                Latitude = incident.Latitude,
                Longitude = incident.Longitude,
                Priority = incident.Priority,
                IncidentPhotos = incident.IncidentPhotos.Select(p => new AnalyticalIncidentPhoto
                {
                    Id = p.Id,
                    IncidentId = p.IncidentId,
                    Url = p.Url,
                    ThumbnailUrl = p.ThumbnailUrl
                }).ToList()
            };
        }
        /*public static AnalyticalIncidentViewModel AnalyticalIncidentToViewModel(AnalyticalIncident incident, List<IncidentsConnection> incidentConnections)
        {
            return new AnalyticalIncidentViewModel
            {
                Id = incident.Id,
                Date = incident.Date,
                Description = incident.Description,
                Type = incident.Type,
                Latitude = incident.Latitude,
                Longitude = incident.Longitude,
                IncidentPhotos = incident.IncidentPhotos.ToList(),
                Priority = incident.Priority,
                IncidentConnections = incidentConnections.Select(c => IncidentsConnectionToViewModel(incident, c)).ToList()
            };
        }*/
        protected static IncidentsConnectionViewModel IncidentsConnectionToViewModel(AnalyticalIncident incident, IncidentsConnection connection)
        {
            bool isFirstInRelation = incident.Id == connection.Incident1Id;
            return new IncidentsConnectionViewModel
            {
                ConnectedIncidentId = (isFirstInRelation ? connection.Incident2Id : connection.Incident1Id),
                ConnectionType = IncidentsConnectionToUnary((IncidentsConnectionType) connection.ConnectionTypeId, isFirstInRelation)
            };
        }
        public static IncidentsConnectionUnaryType IncidentsConnectionToUnary (IncidentsConnectionType connection, bool incidentFirstInRelation)
        {
            if (connection == IncidentsConnectionType.Duplicate)
            {
                if (incidentFirstInRelation)
                    return IncidentsConnectionUnaryType.DubupicatedBy;
                return IncidentsConnectionUnaryType.DublicateOf;
            }
            if (connection == IncidentsConnectionType.ReasonConsequence)
            {
                if (incidentFirstInRelation)
                    return IncidentsConnectionUnaryType.ReasonOf;
                return IncidentsConnectionUnaryType.ConsequenceOf;
            }
            throw new ArgumentException(); 
        }
    }
}
