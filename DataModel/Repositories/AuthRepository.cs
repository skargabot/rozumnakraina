﻿using DataModel.Models;
using DataModel.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace DataModel.Repositories
{
    public class AuthRepository : BaseRepository
    {
        private UserManager<ApplicationUser> _userManager;

        public AuthRepository()
        {
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(UserViewModel userModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            if (result.Succeeded)
            {
                var result1 = _userManager.AddToRoleAsync(user.Id, "Dispatcher");
            }
            return result;
        }

        /*public async Task<IdentityResult> RegisterExternalAppUser(ExternalAppUserViewModel userModel)
        {
            ExternalApp app = new ExternalApp { Name = userModel.AppName };
            _ctx.ExternalApps.Add(app);
            _ctx.SaveChanges();
            ApplicationUser user = new ApplicationUser
            {
                UserName = userModel.UserName,
                ExternalAppId = app.Id
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            if (result.Succeeded)
            {
                var result1 = _userManager.AddToRoleAsync(user.Id, "ExternalApp");
            }
            return result;
        }*/

        /*public bool AttachUserToCompany(string userId, int companyId)
        {
            Company company = _ctx.Companies.Find(companyId);
            if (company == null)
                return false;
            ApplicationUser user = _userManager.FindById(userId);
            if (user == null)
                return false;
            user.CompanyId = companyId;
            _ctx.SaveChanges();
            return true;
        }*/

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public new void Dispose()
        {
            base.Dispose();
            _userManager.Dispose();
        }
    }
}
