﻿using DataModel.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Repositories
{
    public class IncidentStatesMapper : BaseRepository
    {
        public static IncidentStateInternal MapPublicStateToInternal(IncidentStatePublic publicState)
        {
            switch (publicState)
            {
                case IncidentStatePublic.New:
                    return IncidentStateInternal.New;
                case IncidentStatePublic.Read:
                    return IncidentStateInternal.New;
                case IncidentStatePublic.InWork:
                    return IncidentStateInternal.Assigned;
                case IncidentStatePublic.WorkDone:
                    return IncidentStateInternal.Completed;
                case IncidentStatePublic.WorkStopped:
                    return IncidentStateInternal.Postponed;
                case IncidentStatePublic.CheckNeeded:
                    return IncidentStateInternal.Revised;
                case IncidentStatePublic.Closed:
                    return IncidentStateInternal.Closed;
                case IncidentStatePublic.Reopened:
                    return IncidentStateInternal.Reopened;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
