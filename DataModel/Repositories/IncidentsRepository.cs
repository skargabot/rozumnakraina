﻿using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataModel.Enums;
using DataModel.Extentions;
using DataModel.ViewModels;

namespace DataModel.Repositories
{
    public class IncidentsRepository : BaseRepository
    {
        public IQueryable<Incident> GetIncidents()
        {
            return _ctx.Incidents.Include(t => t.User).Include(t => t.IncidentPhotos).Include(t => t.IncidentMentions);
        }

        public Incident GetIncidentById(int id)
        {
            return _ctx.Incidents.Include(t => t.User).Include(t => t.IncidentPhotos).Include(t => t.IncidentMentions).SingleOrDefault(x => x.Id == id);
        }

        public Incident FindIncident(int id)
        {
            return _ctx.Incidents.Find(id);
        }

        public void SetIncidentState(Incident incident, IncidentStatePublic state)
        {
            incident.State = state;
            incident.StateInternal = IncidentStatesMapper.MapPublicStateToInternal(state);
            //CreateincidentHistoryEntity(incident.Id, string.Format("Стан інцинденту змінено на {0}", state.GetDescription()));
            _ctx.SaveChanges();
        }

        public IncidentsStatsResult GetIncidentsStatistics()
        {
            DbSet<Incident> incidents = _ctx.Incidents;
            return new IncidentsStatsResult
            {
                All = incidents.Count(),
                Today = incidents.Count(i => i.Date.Date == DateTime.Today),
                TodayCritical = incidents.Count(i => i.Date.Date == DateTime.Today && i.Priority == IncidentPriority.Critical),
                Closed = incidents.Count(i => i.State == IncidentStatePublic.Closed)
            };
        }

        public void CreateIncidentMention(IncidentMention mention)
        {
            _ctx.IncidentMentions.Add(mention);
            _ctx.SaveChanges();
        }

        public ICollection<IncidentHistoryEntity> GetIncidentHistory(Incident incident)
        {
            return incident.IncidentHistory;
        }

        /*protected void CreateincidentHistoryEntity(int incidentId, string eventDescription)
        {
            _ctx.IncidentHistoryEntyties.Add(new IncidentHistoryEntity
            {
                IncidentId = incidentId,
                Event = eventDescription,
                Date = DateTime.Now
            });
        }*/
    }
}
