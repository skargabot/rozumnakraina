﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataModel;
using DataModel.Models.RozumnaKraina;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер")]
    public class TransportsController : BaseController
    {
        public TransportsController(SkargaBotContext db) : base(db)
        {
        }

        public async Task<JsonResult> GetRoutes(int type)
        {
            var routes = await Db.RKDTransportRoutes.Where(_ => _.TypeId == type).Select(_ => new {_.Id, _.Name}).ToListAsync();
            return Json(routes, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Index()
        {
            var model = await Db.RKDTransports.ProjectToListAsync<TransportViewModel>();

            var rKDTransports = Db.RKDTransports.Include(d => d.Route).Include(d => d.Type);
            var transportsViewModel = await rKDTransports.Select(_ => new TransportViewModel
            {
                Id = _.Id,
                Code = _.Code,
                Route = new TransportRouteViewModel
                {
                    Name = _.Route.Name,
                    Id = _.Route.Id,
                    StartStation = _.Route.StartStation,
                    StopStation = _.Route.StopStation
                },
                Type = new TransportTypeViewModel
                {
                    Id = _.Type.Id,
                    Name = _.Type.Name
                }
            }).ToListAsync();

            return View(transportsViewModel);
        }

        public async Task<ActionResult> Add()
        {
            /*var routes = Db.RKDTransportRoutes
               .Select(s => new
               {
                   Id = s.Id,
                               //should be researched how to display this field in dropdown, the value might be too long
                               Description = s.StartStation + " - " + s.StopStation
               }).ToList();*/

            ViewBag.RouteId = new SelectList(Db.RKDTransportRoutes, "Id", "name");
            ViewBag.TypeId = new SelectList(Db.RKDTransportTypes, "Id", "Name");

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(TransportViewModel model)
        {
            var item = Mapper.Map<DemoTransport>(model);

            if (model.Route.Id != null)
            {
                item.Route.TypeId = item.TypeId;
                Db.RKDTransportRoutes.Attach(item.Route);
            }
            else
            {
                item.Route = null;
            }

            Db.RKDTransportTypes.Attach(item.Type);

            Db.RKDTransports.Add(item);
            await Db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item =
                await Db.RKDTransports.ProjectToSingleOrDefaultAsync<DemoTransport, TransportViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            /*var routes = Db.RKDTransportRoutes
               .Select(s => new
               {
                   Id = s.Id,
                   //should be researched how to display this field in dropdown, the value might be too long
                   Description = s.StartStation + " - " + s.StopStation
               }).ToList();*/

            ViewBag.RouteId = new SelectList(Db.RKDTransportRoutes, "Id", "Name", item.Route?.Id);
            ViewBag.TypeId = new SelectList(Db.RKDTransportTypes, "Id", "Name", item.Type.Id);

            return View(item);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(TransportViewModel model)
        {
            var item = Mapper.Map<DemoTransport>(model);

            if (item == null)
                return HttpNotFound();

            if (model.Route.Id != null)
                Db.RKDTransportRoutes.Attach(item.Route);
            else
            {
                item.Route = null;
            }

            Db.RKDTransportTypes.Attach(item.Type);

            Db.Entry(item).State = EntityState.Modified;

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.RKDTransports.SingleOrDefaultAsync(_ => _.Id == id);

            //TODO should be fixed (remove TypeId field from Route table)
            item.Route.TypeId = item.TypeId;

            if (item == null)
                return HttpNotFound();

            Db.RKDTransports.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}