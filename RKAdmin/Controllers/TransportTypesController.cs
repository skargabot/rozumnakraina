﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataModel;
using DataModel.Models.RozumnaKraina;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер")]
    public class TransportTypesController : BaseController
    {
        public TransportTypesController(SkargaBotContext db) : base(db)
        {
        }

        public async Task<ActionResult> Index()
        {
            var model = await Db.RKDTransportTypes.ProjectToListAsync<TransportTypeViewModel>();
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var item =
                await Db.RKDTransportTypes.ProjectToSingleOrDefaultAsync<DemoTransportType, TransportTypeViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(TransportTypeViewModel model)
        {
            var item = Mapper.Map<DemoTransportType>(model);

            Db.RKDTransportTypes.Add(item);
            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item =
                await Db.RKDTransportTypes.ProjectToSingleOrDefaultAsync<DemoTransportType, TransportTypeViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(IncidentTypeViewModel model)
        {
            var item = await Db.RKDTransportTypes.SingleOrDefaultAsync(_ => _.Id == model.Id);

            if (item == null)
                return HttpNotFound();

            Db.Entry(item).CurrentValues.SetValues(model);

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.RKDTransportTypes.SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();

            Db.RKDTransportTypes.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}