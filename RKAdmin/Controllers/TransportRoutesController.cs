﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataModel;
using DataModel.Models.RozumnaKraina;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер")]
    public class TransportRoutesController : BaseController
    {
        public TransportRoutesController(SkargaBotContext db) : base(db)
        {
        }

        public async Task<ActionResult> Index()
        {
            var model = await Db.RKDTransportRoutes.ProjectToListAsync<TransportRouteViewModel>();

            /*var rKDTransports = Db.RKDTransportRoutes.Include(d => d.TransportOgranization).Include(d => d.Type);
            var transportsViewModel = await rKDTransports.Select(_ => new TransportRouteViewModel
            {
                Id = _.Id,
                StartStation = _.StartStation,
                StopStation = _.StopStation,

                TransportOgranization = new TransportOrganizationModel
                {
                    Id = _.Id,
                    Name = _.TransportOgranization.Name
                },
                Type = new TransportTypeViewModel
                {
                    Id = _.Type.Id,
                    Name = _.Type.Name
                }
            }).ToListAsync();*/

            return View(model);
        }

        public async Task<ActionResult> Add()
        {
            ViewBag.OrganizationId = new SelectList(Db.TransportOgranizations, "Id", "Name");
            ViewBag.TypeId = new SelectList(Db.RKDTransportTypes, "Id", "Name");

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(TransportRouteViewModel model)
        {
            var item = Mapper.Map<DemoTransportRoute>(model);

            Db.TransportOgranizations.Attach(item.TransportOgranization);
            Db.RKDTransportTypes.Attach(item.Type);

            Db.RKDTransportRoutes.Add(item);
            await Db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item =
                await Db.RKDTransportRoutes.ProjectToSingleOrDefaultAsync<DemoTransportRoute, TransportRouteViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            ViewBag.OrganizationId = new SelectList(Db.TransportOgranizations, "Id", "Name", item.TransportOgranization.Id);
            ViewBag.TypeId = new SelectList(Db.RKDTransportTypes, "Id", "Name", item.Type.Id);

            return View(item);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(TransportRouteViewModel model)
        {
            var item = Mapper.Map<DemoTransportRoute>(model);

            if (item == null)
                return HttpNotFound();

            Db.TransportOgranizations.Attach(item.TransportOgranization);
            Db.RKDTransportTypes.Attach(item.Type);

            Db.Entry(item).State = EntityState.Modified;

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.RKDTransportRoutes.SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();

            Db.RKDTransportRoutes.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}