﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataModel;
using DataModel.Models.RozumnaKraina;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер")]
    public class TransportOrganizationsController: BaseController
    {
        public TransportOrganizationsController(SkargaBotContext db) : base(db)
        {
        }

        public async Task<ActionResult> Index()
        {
            var model = await Db.TransportOgranizations.ProjectToListAsync<TransportOrganizationModel>();
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var item =
                await Db.TransportOgranizations.ProjectToSingleOrDefaultAsync<TransportOgranization, TransportOrganizationModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(TransportOrganizationModel model)
        {
            var item = Mapper.Map<TransportOgranization>(model);

            Db.TransportOgranizations.Add(item);
            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item =
                await Db.TransportOgranizations.ProjectToSingleOrDefaultAsync<TransportOgranization, TransportOrganizationModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(TransportOrganizationModel model)
        {
            var item = await Db.TransportOgranizations.SingleOrDefaultAsync(_ => _.Id == model.Id);

            if (item == null)
                return HttpNotFound();

            Db.Entry(item).CurrentValues.SetValues(model);

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.TransportOgranizations.SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();

            Db.TransportOgranizations.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}