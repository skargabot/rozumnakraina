﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataModel;
using Microsoft.AspNet.Identity;

namespace RKAdmin.Controllers
{
    public class BaseController : Controller
    {
        protected SkargaBotContext Db;

        public string CurrentUserId
        {
            get
            {
                var userId = User.Identity.GetUserId();
                return userId;
            }
        }

        protected BaseController(SkargaBotContext db)
        {
            Db = db;
        }
    }
}