﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataModel;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор")]
    public class UsersController : BaseController
    {
        private UserManager<ApplicationUser> _userManager;

        public UsersController(SkargaBotContext db, UserManager<ApplicationUser> userManager) : base(db)
        {
            _userManager = userManager;
        }

        public async Task<ActionResult> Index()
        {
            var model = await _userManager.Users.Include(_ => _.Roles).Select(_ => new UserViewModel
            {
                Id = _.Id,
                UserName = _.UserName,
                Name = _.Name,
                Role = Db.Roles.Select(role => new RoleViewModel
                {
                    Id = role.Id,
                    Name = role.Name
                }).FirstOrDefault(r => _.Roles.Select(rr => rr.RoleId).Contains(r.Id))
            }).ToListAsync();

            return View(model);
        }

        public async Task<ActionResult> Add()
        {
            var roles = await Db.Roles.ToListAsync();
            ViewBag.Roles = new SelectList(roles, "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(UserViewModel model)
        {
            var user = new ApplicationUser
            {
                UserName = model.UserName,
                Name = model.Name,
                Email = model.UserName
            };

            _userManager.UserValidator = new UserValidator<ApplicationUser>(_userManager)
            {
                AllowOnlyAlphanumericUserNames = false
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var role = Db.Roles.FirstOrDefault(_ => _.Id == model.Role.Id);
                await _userManager.AddToRoleAsync(user.Id, role.Name);
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(string id)
        {
            var item =
                await _userManager.Users.Include(_ => _.Roles).FirstOrDefaultAsync(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            var userRoleId = item.Roles.Select(rr => rr.RoleId).FirstOrDefault();
            var model = new UserViewModel
            {
                Id = item.Id,
                UserName = item.UserName,
                Name = item.Name,
                Role = Db.Roles.Select(r => new RoleViewModel
                {
                    Id = r.Id,
                    Name = r.Name
                }).FirstOrDefault(r => r.Id == userRoleId)
            };

            var roles = await Db.Roles.ToListAsync();
            ViewBag.Roles = new SelectList(roles, "Id", "Name");

            return View(model);
        }

        public async Task<ActionResult> Details(string id)
        {
            var item =
                await _userManager.Users.Include(_ => _.Roles).FirstOrDefaultAsync(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            var userRoleId = item.Roles.Select(rr => rr.RoleId).FirstOrDefault();
            var model = new UserViewModel
            {
                Id = item.Id,
                UserName = item.UserName,
                Name = item.Name,
                Role = Db.Roles.Select(r => new RoleViewModel
                {
                    Id = r.Id,
                    Name = r.Name
                }).FirstOrDefault(r => r.Id == userRoleId)
            };

            var roles = await Db.Roles.ToListAsync();
            ViewBag.Roles = new SelectList(roles, "Id", "Name");

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            var item =
                await _userManager.Users.Include(_ => _.Roles).FirstOrDefaultAsync(_ =>
                    _.Id == model.Id);

            if (item == null)
                return HttpNotFound();

            item.Name = model.Name;
            await _userManager.UpdateAsync(item);

            var userRoleId = item.Roles.Select(rr => rr.RoleId).FirstOrDefault();
            var userRole = await Db.Roles.FirstOrDefaultAsync(_ => _.Id == userRoleId);
            var newRole = Db.Roles.FirstOrDefault(_ => _.Id == model.Role.Id);

            await _userManager.RemoveFromRoleAsync(item.Id, userRole.Name);
            await _userManager.AddToRoleAsync(item.Id, newRole.Name);

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            var item =
                await Db.Users.Include(_ => _.Roles).FirstOrDefaultAsync(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            Db.Users.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}