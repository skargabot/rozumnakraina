﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataModel;
using DataModel.Models.RozumnaKraina;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер")]
    public class IncidentTypesController: BaseController
    {
        public IncidentTypesController(SkargaBotContext db) : base(db)
        {
        }

        public async Task<ActionResult> Index()
        {
            var model = await Db.RKDIncidentTypes.ProjectToListAsync<IncidentTypeViewModel>();
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var item =
                await Db.RKDIncidentTypes.ProjectToSingleOrDefaultAsync<DemoIncidentType, IncidentTypeViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(IncidentTypeViewModel model)
        {
            var item = Mapper.Map<DemoIncidentType>(model);

            Db.RKDIncidentTypes.Add(item);
            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item =
                await Db.RKDIncidentTypes.ProjectToSingleOrDefaultAsync<DemoIncidentType, IncidentTypeViewModel>(_ =>
                    _.Id == id);

            if (item == null)
                return HttpNotFound();

            return View(item);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(IncidentTypeViewModel model)
        {
            var item = await Db.RKDIncidentTypes.SingleOrDefaultAsync(_ => _.Id == model.Id);

            if (item == null)
                return HttpNotFound();

            Db.Entry(item).CurrentValues.SetValues(model);

            await Db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.RKDIncidentTypes.SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();

            Db.RKDIncidentTypes.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}