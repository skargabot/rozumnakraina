﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataModel;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер, Майстер")]
    public class IncidentsController: BaseController
    {
        private UserManager<ApplicationUser> _userManager;

        public IncidentsController(SkargaBotContext db, UserManager<ApplicationUser> userManager) : base(db)
        {
            _userManager = userManager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("All");
        }

        [HttpPost]
        public async Task<ActionResult> PostMessage(IncidentViewModel model)
        {
            var incident = await Db.RKDIncidents.Include(_ => _.User).FirstOrDefaultAsync(_ => _.Id == model.Id);

            if (incident == null)
                return HttpNotFound();

            var message = new IncidentMessage
            {
                Text = model.NewMessage.Text,
                SenderId = User.Identity.GetUserId(),
                IncidentId = model.Id
            };

            Db.IncidentMessages.Add(message);
            await Db.SaveChangesAsync();

            var files = Request.Files;
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    foreach (string file in files)
                    {
                        using (var ms = new MemoryStream())
                        {
                            var postedFile = Request.Files[file];
                            postedFile.InputStream.CopyTo(ms);
                            var streamContent = new ByteArrayContent(ms.ToArray());
                            streamContent.Headers.ContentType = new MediaTypeHeaderValue(postedFile.ContentType);
                            streamContent.Headers.ContentLength = postedFile.ContentLength;
                            streamContent.Headers.Add("Content-Disposition", "form-data; name=\"" + file + "\"; filename=\"" + postedFile.FileName + "\"");
                            content.Add(streamContent, "file", postedFile.FileName);
                        }
                    }

                    await client.PostAsync(MvcApplication.ApiHost + "/api/rk/files/post/message/" + message.Id, content);
                }
            }            

            if (incident.EnableNotifications)
            {
                await EmailHelper.SendMessageEmail(incident, message);
            }

            return RedirectToAction("Incident", new {id = model.Id});
        }

        public async Task<ActionResult> My()
        {
            
            var model = await Db.RKDIncidents
                .Include(_ => _.AssignedUser)
                .Include(_ => _.User)
                .Include(_ => _.TransportRoute)
                .Include(_ => _.Relations.Select(r => r.IncidentType))
                .Where(_ => _.UserId == CurrentUserId || _.AssignedUserId == CurrentUserId)
                .Select(_ => new
                {
                    _.Id,
                    AssignedUser = new
                    {
                        _.AssignedUser.Name,
                        _.AssignedUser.UserName,
                        Role = Db.Roles.FirstOrDefault(r => r.Id == _.AssignedUser.Roles.FirstOrDefault().RoleId)
                    },
                    User = new
                    {
                        _.User.Name,
                        _.User.UserName,
                        Role = Db.Roles.FirstOrDefault(r => r.Id == _.User.Roles.FirstOrDefault().RoleId)
                    },
                    _.TransportType,
                    _.TransportRoute,
                    _.CraetedAt,
                    _.State,
                    IncidentTypes = _.Relations.Select(r => r.IncidentType)
                })
                .ProjectToListAsync<IncidentViewModel>();

            return View(model);
        }

        public async Task<ActionResult> Filter(int[] incidentType, int? route, int? trasportType)
        {
            if (incidentType?.Length == 0 && route == null && trasportType == null)
                return RedirectToAction("Index");

            var query = Db.RKDIncidents
                .Include(_ => _.AssignedUser.Roles)
                .Include(_ => _.User.Roles)
                .Include(_ => _.TransportRoute)
                .Include(_ => _.Relations.Select(r => r.IncidentType));

            if (incidentType?.Length > 0)
            {
                query = query.Where(_ => _.Relations.Select(r => r.IncidentTypeId).Any(i => incidentType.Contains(i)));
            }

            if (route != null)
            {
                query = query.Where(_ => _.TransportRouteId == route);
            }

            if (trasportType != null)
            {
                query = query.Where(_ => _.TransportTypeId == trasportType);
            }

            var model = await query.Select(_ => new
            {
                _.Id,
                AssignedUser = new
                {
                    _.AssignedUser.Id,
                    _.AssignedUser.Name,
                    _.AssignedUser.UserName,
                    Role = Db.Roles.FirstOrDefault(r => r.Id == _.AssignedUser.Roles.FirstOrDefault().RoleId)
                },
                _.TransportType,
                _.TransportRoute,
                _.CraetedAt,
                _.State,
                _.DeadlineDate,
                _.DeadlineState,
                IncidentTypes = _.Relations.Select(r => r.IncidentType),
                User = new
                {
                    _.User.Id,
                    _.User.Name,
                    _.User.UserName,
                    Role = Db.Roles.FirstOrDefault(r => r.Id == _.User.Roles.FirstOrDefault().RoleId)
                }
            }).ProjectToListAsync<IncidentViewModel>();

            return View("All", model);
        }

        public async Task<ActionResult> All()
        {
            var model = await Db.RKDIncidents
                .Include(_ => _.AssignedUser.Roles)
                .Include(_ => _.User.Roles)
                .Include(_ => _.TransportRoute)
                .Include(_ => _.Relations.Select(r => r.IncidentType))
                .Select(_ => new
                {
                    _.Id,
                    AssignedUser = new
                    {
                        _.AssignedUser.Id,
                        _.AssignedUser.Name,
                        _.AssignedUser.UserName,
                        Role = Db.Roles.FirstOrDefault(r => r.Id == _.AssignedUser.Roles.FirstOrDefault().RoleId)
                    },
                    _.TransportType,
                    _.TransportRoute,
                    _.CraetedAt,
                    _.State,
                    _.DeadlineDate,
                    _.DeadlineState,
                    IncidentTypes = _.Relations.Select(r => r.IncidentType),
                    User = new
                    {
                        _.User.Id,
                        _.User.Name,
                        _.User.UserName,
                        Role = Db.Roles.FirstOrDefault(r => r.Id == _.User.Roles.FirstOrDefault().RoleId)
                    }
                })
                .ProjectToListAsync<IncidentViewModel>();

            return View(model);
        }

        public async Task<ActionResult> SelfAssign(int id)
        {
            var incident = await Db.RKDIncidents.FirstOrDefaultAsync(_ => _.Id == id);

            if (incident == null)
                return HttpNotFound();

            incident.AssignedUserId = User.Identity.GetUserId();
            Db.SaveChanges();

            incident = await Db.RKDIncidents.Include(_ => _.AssignedUser).FirstOrDefaultAsync(_ => _.Id == id);
            await EmailHelper.SendAssignedEmail(incident);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Incident(int id)
        {
            var incident = await Db.RKDIncidents
                .Include(_ => _.AssignedUser)
                .Include(_ => _.User)
                .Include(_ => _.TransportRoute)
                .Include(_ => _.Relations.Select(r => r.IncidentType))
                .Where(_ => _.Id == id)
                .Select(_ => new
                {
                    _.Id,
                    _.AssignedUser,
                    _.TransportType,
                    _.TransportRoute,
                    _.Transport,
                    _.User,
                    _.CraetedAt,
                    _.State,
                    _.Images,
                    _.Latitude,
                    _.Longitude,
                    _.Description,
                    _.Messages,
                    _.DeadlineDate,
                    _.DeadlineState,
                    IncidentTypes = _.Relations.Select(r => r.IncidentType)
                })
                .ProjectToSingleOrDefaultAsync<IncidentViewModel>();

            if (incident == null)
                return HttpNotFound();

            var roleIds = Db.Roles.Where(_ => _.Name == "Диспетчер" || _.Name == "Майстер").Select(_ => _.Id);
            ViewBag.Users = await _userManager.Users.Where(u => u.Roles.Any(_ => roleIds.Contains(_.RoleId))).Select(_ => new SelectListItem
            {
                Text = _.Name +  "(" + _.UserName + ")",
                Value = _.Id
            }).ToListAsync();

            return View(incident);
        }

        [HttpPost]
        public async Task<ActionResult> Incident(IncidentViewModel model)
        {
            var incident = await Db.RKDIncidents.FirstOrDefaultAsync(_ => _.Id == model.Id);

            if (incident == null)
                return HttpNotFound();

            var currentState = incident.State;
            var currentAssigne = incident.AssignedUserId;

            incident.State = model.State;
            incident.AssignedUserId = model.AssignedUser.Id;
            incident.DeadlineDate = model.DeadlineDate;
            incident.DeadlineState = model.DeadlineState;
            
            await Db.SaveChangesAsync();

            if (currentState != model.State)
            {
                if (incident.EnableNotifications)
                {
                    await EmailHelper.SendChangeStateEmail(incident);
                }
            }

            if (currentAssigne != model.AssignedUser?.Id)
            {
                incident = await Db.RKDIncidents.Include(_ => _.AssignedUser).FirstOrDefaultAsync(_ => _.Id == model.Id);
                await EmailHelper.SendAssignedEmail(incident);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.RKDIncidents.SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();

            var relations = Db.RKDIncidentTypeRelations.Where(_ => _.IncidentId == id);
            var images = Db.Images.Where(_ => _.RkIncidentId == id);
            var messages = Db.IncidentMessages.Where(_ => _.IncidentId == id);

            Db.IncidentMessages.RemoveRange(messages);
            Db.Images.RemoveRange(images);
            Db.RKDIncidentTypeRelations.RemoveRange(relations);
            Db.RKDIncidents.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}