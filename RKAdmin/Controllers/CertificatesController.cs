﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataModel;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    public class CertificatesController: BaseController
    {
        public CertificatesController(SkargaBotContext db) : base(db)
        {
        }

        public ActionResult Index()
        {
            return RedirectToAction("My");
        }

        [HttpPost]
        public async Task<ActionResult> DeleteItem(int id)
        {
            var item = Db.CertificateItems.Find(id);
            if (item == null)
                return HttpNotFound();

            var certificate = Db.Certificates.Find(item.CertificateId);
            certificate.CertificateSum -= item.Cost * item.Count;

            Db.CertificateItems.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }

        [HttpPost]
        public async Task<ActionResult> DeleteWork(int id)
        {
            var item = Db.CertificateWorks.Find(id);
            if (item == null)
                return HttpNotFound();

            var certificate = Db.Certificates.Find(item.CertificateId);
            certificate.CertificateSum -= item.Cost * item.Count * item.Time;

            Db.CertificateWorks.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }

        public async Task<ActionResult> My()
        {
            var model = await Db.Certificates
                .Include(_ => _.CertificateItems)
                .Include(_ => _.CertificateWorks)
                .Include(_ => _.User)
                .Where(_ => _.UserId == CurrentUserId || _.UserId == CurrentUserId)
                .ProjectToListAsync<CertificateViewModel>();

            return View(model);
        }


        public async Task<ActionResult> All()
        {
            var model = await Db.Certificates
                .Include(_ => _.CertificateItems)
                .Include(_ => _.CertificateWorks)
                .Include(_ => _.User)
                .ProjectToListAsync<CertificateViewModel>();

            return View(model);
        }

        public async Task<ActionResult> Certificate(int id)
        {
            var model = await Db.Certificates
                .Include(_ => _.CertificateItems)
                .Include(_ => _.CertificateWorks)
                .Include(_ => _.User)
                 .ProjectToFirstOrDefaultAsync<Certificate, CertificateViewModel>(_ => _.Id == id);

            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await Db.Certificates
                .Include(_ => _.CertificateItems)
                .Include(_ => _.CertificateWorks)
                .Include(_ => _.User)
                .ProjectToFirstOrDefaultAsync<Certificate, CertificateViewModel>(_ => _.Id == id);

            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        public ActionResult Create(int orderId)
        {
            var model = new CertificateViewModel() { OrderId = orderId };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CertificateViewModel model)
        {
            var entity = Mapper.Map<Certificate>(model);
            entity.CreationDate = TimeZoneHelper.GetFleDateTime();
            entity.UserId = User.Identity.GetUserId();

            if (model.CertificateItems != null)
            {
                var itemsSum = model.CertificateItems.Sum(_ => _.Cost * _.Count);
                entity.CertificateSum += itemsSum;
            }

            if (model.CertificateWorks != null)
            {
                var worksSum = model.CertificateWorks.Sum(_ => _.Count * _.Cost * _.Time);
                entity.CertificateSum += worksSum;
            }

            Db.Certificates.Add(entity);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditWorkRow(CertificateWorkViewModel model)
        {
            var item = Db.CertificateWorks.Find(model.Id);
            if (item == null)
                return HttpNotFound();

            var certificate = Db.Certificates.Find(item.CertificateId);
            var oldSum = item.Cost * item.Count * item.Time;
            var newsum = model.Cost * model.Count * model.Time;

            Db.Entry(item).CurrentValues.SetValues(model);
            item.CertificateId = certificate.Id;

            certificate.CertificateSum += newsum - oldSum;

            Db.SaveChanges();
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult EditItemRow(CertificateItemViewModel model)
        {
            var item = Db.CertificateItems.Find(model.Id);
            if (item == null)
                return HttpNotFound();

            var certificate = Db.Certificates.Find(item.CertificateId);
            var oldSum = item.Cost * item.Count;
            var newsum = model.Cost * model.Count;

            Db.Entry(item).CurrentValues.SetValues(model);
            item.CertificateId = certificate.Id;

            certificate.CertificateSum += newsum - oldSum;

            Db.SaveChanges();
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult Edit(CertificateViewModel model)
        {
            var current = Db.Certificates.Find(model.Id);
            if (current == null)
                return HttpNotFound();

            var entity = Mapper.Map<Certificate>(model);       

            if (model.CertificateItems != null)
            {
                var itemsSum = model.CertificateItems.Sum(_ => _.Cost * _.Count);
                entity.CertificateSum += itemsSum;
            }

            if (model.CertificateWorks != null)
            {
                var worksSum = model.CertificateWorks.Sum(_ => _.Count * _.Cost * _.Time);
                entity.CertificateSum += worksSum;
            }


            current.CertificateSum += entity.CertificateSum;
            entity.CertificateItems.ForEach(_ =>
            {
                _.CertificateId = current.Id;
                Db.CertificateItems.Add(_);
            });
            entity.CertificateWorks.ForEach(_ =>
            {
                _.CertificateId = current.Id;
                Db.CertificateWorks.Add(_);
            });

            Db.SaveChanges();
            return RedirectToAction("Certificate", new { id = model.Id});
        }
    }
}