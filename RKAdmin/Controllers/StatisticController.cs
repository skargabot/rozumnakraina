﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataModel;
using DataModel.Enums;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    public class StatisticController: BaseController
    {
        public StatisticController(SkargaBotContext db) : base(db)
        {
        }

        public ActionResult Summary()
        {
            var model = new SummaryStatisticViewModel();

            model.ByTypes = Db.RKDIncidentTypes.Include(_ => _.Relations.Select(r => r.Incident)).Select(_ => new StatisticItem
            {
                Text = _.Name,
                ItemId = _.Id,
                Value = _.Relations.Count,
                New = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.New),
                Read = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.Read),
                InWork = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.InWork),
                WorkDone = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.WorkDone),
                WorkStopped = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.WorkStopped),
                CheckNeeded = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.CheckNeeded),
                Closed = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.Closed),
                Reopened = _.Relations.Count(i => i.Incident.State == IncidentStatePublic.Reopened),

            }).OrderByDescending(_ => _.Value).ToList();

            model.ByRoutes = Db.RKDTransportRoutes.Select(_ => new StatisticItem
            {
                Text = _.Name,
                ItemId = _.Id,
                Value = Db.RKDIncidents.Count(i => i.TransportRouteId == _.Id),
                New = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.New && i.TransportRouteId == _.Id),
                Read = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Read && i.TransportRouteId == _.Id),
                InWork = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.InWork && i.TransportRouteId == _.Id),
                WorkDone = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.WorkDone),
                WorkStopped = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.WorkStopped && i.TransportRouteId == _.Id),
                CheckNeeded = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.CheckNeeded && i.TransportRouteId == _.Id),
                Closed = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Closed && i.TransportRouteId == _.Id),
                Reopened = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Reopened && i.TransportRouteId == _.Id),
            }).OrderByDescending(_ => _.Value).ToList();

            model.ByTrasports = Db.RKDTransportTypes.Select(_ => new StatisticItem
            {
                Text = _.Name,
                ItemId = _.Id,
                Value = Db.RKDIncidents.Count(i => i.TransportTypeId == _.Id),
                New = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.New && i.TransportTypeId == _.Id),
                Read = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Read && i.TransportTypeId == _.Id),
                InWork = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.InWork && i.TransportTypeId == _.Id),
                WorkDone = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.WorkDone && i.TransportTypeId == _.Id),
                WorkStopped = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.WorkStopped && i.TransportTypeId == _.Id),
                CheckNeeded = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.CheckNeeded && i.TransportTypeId == _.Id),
                Closed = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Closed && i.TransportTypeId == _.Id),
                Reopened = Db.RKDIncidents.Count(i => i.State == IncidentStatePublic.Reopened && i.TransportTypeId == _.Id),
            }).OrderByDescending(_ => _.Value).ToList();

            return View(model);
        }
    }
}