﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataModel;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using RKAdmin.Models;

namespace RKAdmin.Controllers
{
    [Authorize(Roles = "Адміністратор, Диспетчер, Майстер")]
    public class OrdersController: BaseController
    {
        public OrdersController(SkargaBotContext db) : base(db)
        {
        }

        public ActionResult Index()
        {
            return RedirectToAction("My");
        }

        [HttpPost]
        public ActionResult EditWorkRow(OrderWorkViewModel model)
        {
            var item = Db.OrderWorks.Find(model.Id);
            if (item == null)
                return HttpNotFound();

            var order = Db.Orders.Find(item.OrderId);
            var oldSum = item.Cost * item.Count * item.Time;
            var newsum = model.Cost * model.Count * model.Time;

            Db.Entry(item).CurrentValues.SetValues(model);
            item.OrderId = order.Id;

            order.OrderSum += newsum - oldSum;

            Db.SaveChanges();
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult EditItemRow(OrderItemViewModel model)
        {
            var item = Db.OrderItems.Find(model.Id);
            if (item == null)
                return HttpNotFound();

            var order = Db.Orders.Find(item.OrderId);
            var oldSum = item.Cost * item.Count;
            var newsum = model.Cost * model.Count;

            Db.Entry(item).CurrentValues.SetValues(model);
            item.OrderId = order.Id;

            order.OrderSum += newsum - oldSum;

            Db.SaveChanges();
            return new EmptyResult();
        }

        [HttpPost]
        public async Task<ActionResult> DeleteItem(int id)
        {
            var item = Db.OrderItems.Find(id);
            if (item == null)
                return HttpNotFound();

            var order = Db.Orders.Find(item.OrderId);
            order.OrderSum -= item.Cost * item.Count;

            Db.OrderItems.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }

        [HttpPost]
        public async Task<ActionResult> DeleteWork(int id)
        {
            var item = Db.OrderWorks.Find(id);
            if (item == null)
                return HttpNotFound();

            var order = Db.Orders.Find(item.OrderId);
            order.OrderSum -= item.Cost * item.Count;

            Db.OrderWorks.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }

        public async Task<ActionResult> My()
        {
            var model = await Db.Orders
                .Include(_ => _.OrderWorks)
                .Include(_ => _.OrderItems)
                .Include(_ => _.User)
                .Include(_ => _.AssignedUser)
                .Where(_ => _.AssignedUserId == CurrentUserId || _.UserId == CurrentUserId)
                .ProjectToListAsync<OrderViewModel>();

            return View(model);
        }


        public async Task<ActionResult> All()
        {
            var model = await  Db.Orders
                .Include(_ => _.OrderWorks)
                .Include(_ => _.OrderItems)
                .Include(_ => _.User)
                .Include(_ => _.AssignedUser)
                .ProjectToListAsync<OrderViewModel>();

            return View(model);
        }

        public async Task<ActionResult> Order(int id)
        {
            var model = await Db.Orders
                .Include(_ => _.OrderWorks)
                .Include(_ => _.OrderItems)
                .Include(_ => _.User)
                .Include(_ => _.AssignedUser)
                .ProjectToFirstOrDefaultAsync<Order, OrderViewModel>(_ => _.Id == id);

            return View(model);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var model = await Db.Orders
                .Include(_ => _.OrderWorks)
                .Include(_ => _.OrderItems)
                .Include(_ => _.User)
                .Include(_ => _.AssignedUser)
                .ProjectToFirstOrDefaultAsync<Order, OrderViewModel>(_ => _.Id == id);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(OrderViewModel model)
        {
            var order = Db.Orders.Find(model.Id);
            if (order == null)
                return HttpNotFound();

            var entity = Mapper.Map<Order>(model);
            entity.AssignedUserId = order.AssignedUserId;
            entity.UserId = order.UserId;
            entity.CreatedAt = order.CreatedAt;
            entity.IncidentId = order.IncidentId;
            entity.OrderSum = order.OrderSum;

            decimal sum = 0;
            if (model.OrderItems != null)
            {
                var itemsSum = model.OrderItems.Sum(_ => _.Cost * _.Count);
                sum += itemsSum;
            }

            if (model.OrderWorks != null)
            {
                var worksSum = model.OrderWorks.Sum(_ => _.Count * _.Cost * _.Time);
                sum += worksSum;
            }

            entity.OrderSum += sum;
            Db.Entry(order).CurrentValues.SetValues(entity);

            entity.OrderItems.ForEach(_ =>
            {
                _.OrderId = order.Id;
                Db.OrderItems.Add(_);
            });
            entity.OrderWorks.ForEach(_ =>
            {
                _.OrderId = order.Id;
                Db.OrderWorks.Add(_);
            });

            await Db.SaveChangesAsync();
            return RedirectToAction("Order", new {id = order.Id});
        }

        public ActionResult Create(int incidentId)
        {
            var model = new OrderViewModel {IncidentId = incidentId};
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(OrderViewModel model)
        {
            var entity = Mapper.Map<Order>(model);
            entity.CreatedAt = TimeZoneHelper.GetFleDateTime();
            entity.UserId = User.Identity.GetUserId();

            //var dispatcher = Db.Users.Include(_ => _.Roles).FirstOrDefault(_ =>
            //    _.Roles.Any(r => r.RoleId == Db.Roles.FirstOrDefault(rr => rr.Name == "Диспетчер").Id));

            entity.AssignedUserId = User.Identity.GetUserId();

            if (model.OrderItems != null)
            {
                var itemsSum = model.OrderItems.Sum(_ => _.Cost * _.Count);
                entity.OrderSum += itemsSum;
            }

            if (model.OrderWorks != null)
            {
                var worksSum = model.OrderWorks.Sum(_ => _.Count * _.Cost * _.Time);
                entity.OrderSum += worksSum;
            }

            Db.Orders.Add(entity);
            Db.SaveChanges();
            return RedirectToAction("My");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await Db.Orders.Include(_ => _.OrderWorks).Include(_ => _.OrderItems).SingleOrDefaultAsync(_ => _.Id == id);

            if (item == null)
                return HttpNotFound();


            Db.OrderWorks.RemoveRange(item.OrderWorks);
            Db.OrderItems.RemoveRange(item.OrderItems);
            Db.Orders.Remove(item);
            await Db.SaveChangesAsync();
            return new EmptyResult();
        }
    }
}