﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using DataModel;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace RKAdmin
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            // получаем экземпляр контейнера
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterFilterProvider();

            builder.Register(c => new SkargaBotContext()).InstancePerLifetimeScope();

            var dbContextParameter = new ResolvedParameter((pi, ctx) => pi.ParameterType == typeof(DbContext),
                (pi, ctx) => ctx.Resolve<SkargaBotContext>());

            builder.RegisterType<UserStore<ApplicationUser>>().As<IUserStore<ApplicationUser>>().WithParameter(dbContextParameter).InstancePerLifetimeScope();
            //builder.RegisterType<RoleStore<IdentityRole>>().As<IRoleStore<IdentityRole>>().WithParameter(dbContextParameter).InstancePerLifetimeScope();

            builder.RegisterType<UserManager<ApplicationUser>>();
            //builder.RegisterType<RoleManager<IdentityRole>>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}