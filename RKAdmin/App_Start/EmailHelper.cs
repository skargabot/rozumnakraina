﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using DataModel.Extentions;
using DataModel.Models.RozumnaKraina;

namespace RKAdmin
{
    public static class EmailHelper
    {
        public static async Task<bool> SendChangeStateEmail(DemoIncident incident)
        {
            var userEmail = incident.User != null ? incident.User.Email ?? incident.User.UserName : incident.Email;

            if (string.IsNullOrEmpty(userEmail))
                return false;

            var templatePath = HttpContext.Current.Server.MapPath("~/App_Data/changestate_template.html");
            var templateData = File.ReadAllText(templatePath);
            templateData = templateData.Replace("{name}", incident.User?.Name ?? userEmail);
            templateData = templateData.Replace("{incidentId}", incident.Id.ToString());
            templateData = templateData.Replace("{incidentState}", incident.State.GetDisplayName());
            
            return await SendEmail(userEmail, "Нове повідомлення стосовно інциденту", templateData, true);
        }

        public static async Task<bool> SendAssignedEmail(DemoIncident incident)
        {
            if (string.IsNullOrEmpty(incident.AssignedUser?.Email))
                return false;

            var templatePath = HttpContext.Current.Server.MapPath("~/App_Data/assigne_template.html");
            var templateData = File.ReadAllText(templatePath);
            templateData = templateData.Replace("{name}", incident.AssignedUser?.Name ?? incident.AssignedUser.Email);
            templateData = templateData.Replace("{incidentId}", incident.Id.ToString());
            templateData = templateData.Replace("{incidentState}", incident.State.GetDisplayName());

            return await SendEmail(incident.AssignedUser.Email, "Вас зроблено вiдповiдальним по інциденту", templateData, true);
        }

        public static async Task<bool> SendMessageEmail(DemoIncident incident, IncidentMessage newMessage)
        {
            var userEmail = incident.User != null ? incident.User.Email ?? incident.User.UserName : incident.Email;

            if (string.IsNullOrEmpty(userEmail))
                return false;

            var templatePath = HttpContext.Current.Server.MapPath("~/App_Data/comment_template.html");
            var templateData = File.ReadAllText(templatePath);
            templateData = templateData.Replace("{comment}", newMessage.Text);
            templateData = templateData.Replace("{name}", incident.User?.Name ?? userEmail);
            templateData = templateData.Replace("{incidentId}", incident.Id.ToString());
            templateData = templateData.Replace("{incidentState}", incident.State.GetDisplayName());
            
            return await SendEmail(userEmail, "Нове повідомлення стосовно інциденту", templateData, true);
        }

        public static async Task<bool> SendEmail(string address, string subject, string body, bool isHtml = false, params Attachment[] attachments)
        {
            var result = await Task.Run(() =>
            {
                try
                {
                    var mailMessage = new MailMessage
                    {
                        Subject = subject,
                        Body = body,
                        From = new MailAddress("rozumnakraina@gmail.com", "Розумна Країна"),
                        To = { address },
                        IsBodyHtml = isHtml
                    };

                    if (attachments != null && attachments.Length > 0)
                        foreach (var a in attachments)
                        {
                            mailMessage.Attachments.Add(a);
                        }

                    using (var client = new SmtpClient())
                    {
                        client.Host = "smtp.gmail.com";
                        client.Port = 587;
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential
                        {
                            UserName = "rozumnakraina@gmail.com",
                            Password = "Q1234567q!"
                        };

                        client.Send(mailMessage);
                    }

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            });

            return result;
        }
    }
}