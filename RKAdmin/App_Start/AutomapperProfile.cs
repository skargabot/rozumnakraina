﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using Microsoft.AspNet.Identity.EntityFramework;
using RKAdmin.Models;

namespace RKAdmin
{
    public class AutomapperProfile: Profile
    {
        public AutomapperProfile()
        {
            CreateMap<DemoIncidentType, IncidentTypeViewModel>().ReverseMap();
            CreateMap<DemoTransportType, TransportTypeViewModel>().ReverseMap();
            CreateMap<TransportOgranization, TransportOrganizationModel>().ReverseMap();
            CreateMap<ApplicationUser, UserViewModel>().ReverseMap();
            CreateMap<IdentityUserRole, RoleViewModel>().ReverseMap();
            CreateMap<DemoTransport, TransportViewModel>().ReverseMap();
            CreateMap<DemoTransportRoute, TransportRouteViewModel>().ReverseMap();
            CreateMap<Order, OrderViewModel>().ReverseMap();
            CreateMap<OrderWork, OrderWorkViewModel>().ReverseMap();
            CreateMap<OrderItem, OrderItemViewModel>().ReverseMap();
            CreateMap<Certificate, CertificateViewModel>().ReverseMap();
            CreateMap<CertificateItem, CertificateItemViewModel>().ReverseMap();
            CreateMap<CertificateWork, CertificateWorkViewModel>().ReverseMap();
        }
    }
}