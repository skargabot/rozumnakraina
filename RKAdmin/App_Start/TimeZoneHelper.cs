﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin
{
    public static class TimeZoneHelper
    {
        public static DateTime GetFleDateTime()
        {
            var fleDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(
                DateTime.UtcNow, "FLE Standard Time");
            return fleDateTime;
        }
    }
}