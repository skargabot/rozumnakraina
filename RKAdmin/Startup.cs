﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RKAdmin.Startup))]
namespace RKAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
