﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DataModel.Enums;

namespace RKAdmin.Models
{
    public class CertificateViewModel
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public UserViewModel User { get; set; }
        public int OrderId { get; set; }
        public decimal CertificateSum { get; set; }

        public List<CertificateItemViewModel> CertificateItems { get; set; }
        public List<CertificateWorkViewModel> CertificateWorks { get; set; }
    }
}