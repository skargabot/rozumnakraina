﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class CertificateWorkViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public int Time { get; set; }
        public decimal Cost { get; set; }
    }
}