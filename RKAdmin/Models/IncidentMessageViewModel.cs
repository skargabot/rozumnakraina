﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class IncidentMessageViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
        public UserViewModel Sender { get; set; }
        public List<ImageViewModel> Images { get; set; }
    }
}