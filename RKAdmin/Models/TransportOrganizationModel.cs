﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class TransportOrganizationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}