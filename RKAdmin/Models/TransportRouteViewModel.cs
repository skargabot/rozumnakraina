﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class TransportRouteViewModel
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string StartStation { get; set; }

        public string StopStation { get; set; }

        public TransportOrganizationModel TransportOgranization { get; set; }

        public TransportTypeViewModel Type { get; set; }
    }
}