﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class TransportViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public TransportRouteViewModel Route { get; set; }

        public TransportTypeViewModel Type { get; set; }

    }
}