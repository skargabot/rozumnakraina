﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class IncidentTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool ForDrivers { get; set; }
    }
}