﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel.Enums;

namespace RKAdmin.Models
{
    public class IncidentViewModel
    {
        public int Id { get; set; }
        public DateTime CraetedAt { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public IncidentStatePublic State { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public IncidentStatePublic? DeadlineState { get; set; }

        public IncidentMessageViewModel NewMessage { get; set; }
        public UserViewModel AssignedUser { get; set; }
        public UserViewModel User { get; set; }
        public TransportTypeViewModel TransportType { get; set; }
        public TransportRouteViewModel TransportRoute { get; set; }
        public TransportViewModel Transport { get; set; }

        public List<IncidentTypeViewModel> IncidentTypes { get; set; }
        public List<ImageViewModel> Images { get; set; }
        public List<IncidentMessageViewModel> Messages { get; set; }
    }
}