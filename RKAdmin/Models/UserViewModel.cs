﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        
        public RoleViewModel Role { get; set; }
    }
}