﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class ImageViewModel
    {
        public string Id { get; set; }
        public string ContentType { get; set; }
    }
}