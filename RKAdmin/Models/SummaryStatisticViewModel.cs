﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class SummaryStatisticViewModel
    {
        public List<StatisticItem> ByTypes { get; set; }
        public List<StatisticItem> ByTrasports { get; set; }
        public List<StatisticItem> ByRoutes { get; set; }
    }
}