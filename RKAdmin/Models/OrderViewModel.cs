﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DataModel.Enums;

namespace RKAdmin.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        public int IncidentId { get; set; }
        public string AutoBarnd { get; set; }
        public string AutoMark { get; set; }
        public int AutoYear { get; set; }
        [DataType(DataType.Date)]
        public DateTime WorkStart { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime WorkEnd { get; set; } = DateTime.Now;
        public UserViewModel User { get; set; }
        public UserViewModel AssignedUser { get; set; }
        public decimal OrderSum { get; set; }
        public OrderState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public string WorkPlan { get; set; }

        public List<OrderWorkViewModel> OrderWorks { get; set; }
        public List<OrderItemViewModel> OrderItems { get; set; }
    }
}