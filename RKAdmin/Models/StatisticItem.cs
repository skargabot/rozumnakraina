﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RKAdmin.Models
{
    public class StatisticItem
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public int ItemId { get; set; }
        public int New { get; set; }
        public int Read { get; set; }
        public int InWork { get; set; }
        public int WorkDone { get; set; }
        public int WorkStopped { get; set; }
        public int CheckNeeded { get; set; }
        public int Closed { get; set; }
        public int Reopened { get; set; }
    }
}