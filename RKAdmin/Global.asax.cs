﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RKAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //public const string ApiHost = "http://localhost:57094";
        public const string ApiHost = "http://testserver.unicreo.com:8090";

        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutoMapper.Mapper.Initialize(_ => _.AddProfile(new AutomapperProfile()));
        }
    }
}
