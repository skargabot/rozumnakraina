﻿using DataModel;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using DataModel.Enums;
using DataModel.Models;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using User = DataModel.Models.User;
namespace Bot
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
/*
namespace Bot
{
    class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient("295492480:AAEK203gLLNg0y_JUIML6NKM1jPrPZ_pXo8");

        private static SkargaBotContext db = new SkargaBotContext(ConfigurationManager.ConnectionStrings["DebugConnection"].ConnectionString);
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException +=
                (s, e) => Console.WriteLine((e.ExceptionObject as Exception)?.Message);

            Bot.OnMessage += BotOnMessageReceived;

            var me = Bot.GetMeAsync().Result;

            Bot.StartReceiving();
            Console.WriteLine("Bot started: " + me.Username);
            Console.ReadLine();
            Bot.StopReceiving();
        }
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            try
            {
                if (message == null) return;


                int? incidentId = GetIncidentIdByChannelId(message.Chat.Id);

                if (incidentId == null)
                {
                    if (message.Type == MessageType.TextMessage)
                    {
                        if (message.Text.Equals("/start"))
                        {
                            await
                                Bot.SendTextMessageAsync(message.Chat.Id,
                                    "Вітаю! Щоб створити скаргу почніть з опису або додайте фото");
                            return;
                        }

                        User user = GetUserByChannelId(message.Chat.Id);
                        if (user == null)
                        {
                            user = new User {ChatId = message.Chat.Id};
                            //db.ExternalUsers.Add(user);
                        }
                        Incident incident = new Incident
                        {
                            Date = DateTime.Now,
                            Description = message.Text,
                            User = user,
                            State = IncidentStatePublic.New,
                            CreationStep = CreateReportStepsEnum.CreationApproveWithDescription
                        };
                        db.Incidents.Add(incident);
                        db.SaveChanges();
                        user.CreatingIncidentId = incident.Id;
                        db.SaveChanges();
                        await
                            Bot.SendTextMessageAsync(message.Chat.Id, "Створити звіт з таким описом?",
                                replyMarkup: GetYesNoKeyboard());
                        return;
                    }
                    if (message.Type == MessageType.PhotoMessage)
                    {
                        User user = GetUserByChannelId(message.Chat.Id);
                        if (user == null)
                        {

                            user = new User {ChatId = message.Chat.Id};
                            //db.ExternalUsers.Add(user);
                        }

                        var file = await Bot.GetFileAsync(message.Photo.LastOrDefault()?.FileId);

                        var filename = file.FileId + "." + file.FilePath.Split('.').Last();
                        var virtualpath = "/bot/uploads/reports/" + filename;
                        var path = @".\uploads\reports\" + filename;
                        using (var saveImageStream = System.IO.File.Open(path, FileMode.Create))
                        {
                            await file.FileStream.CopyToAsync(saveImageStream);
                        }

                        Incident incident = new Incident
                        {
                            Date = DateTime.Now,
                            User = user,
                            State = IncidentStatePublic.New,
                            CreationStep = CreateReportStepsEnum.CreationApproveWithPhoto
                        };
                        db.Incidents.Add(incident);
                        IncidentPhoto photo = new IncidentPhoto
                        {
                            Url = virtualpath,
                            ThumbnailUrl = virtualpath,
                            Incident = incident
                        };
                        db.IncidentPhotos.Add(photo);
                        db.SaveChanges();
                        user.CreatingIncidentId = incident.Id;
                        db.SaveChanges();
                        await
                            Bot.SendTextMessageAsync(message.Chat.Id, "Створити звіт і прив`язати фото?",
                                replyMarkup: GetYesNoKeyboard());
                    }

                }
                else
                {
                    var user = GetUserByChannelId(message.Chat.Id);
                    var incident = GetIncidentById(incidentId.Value);
                    if (message.Type == MessageType.TextMessage)
                    {
                        switch (incident.CreationStep)
                        {
                            case CreateReportStepsEnum.CreationApproveWithDescription:
                                if (message.Text.ToLower().Equals("так"))
                                {
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id, "Бажаєте додати фото?",
                                            replyMarkup: GetYesNoKeyboard());
                                    incident.CreationStep = CreateReportStepsEnum.EnterPhoto;
                                    db.SaveChanges();
                                    break;
                                }

                                if (message.Text.ToLower().Equals("ні"))
                                {
                                    incident.CreationStep = CreateReportStepsEnum.Canceled;
                                    user = GetUserByChannelId(message.Chat.Id);
                                    user.CreatingIncidentId = null;
                                    db.SaveChanges();
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id, "Ваш звіт скасовано",
                                            replyMarkup: new ReplyKeyboardHide());
                                    break;
                                }
                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id, "Створити звіт з таким описом?",
                                        replyMarkup: GetYesNoKeyboard());
                                break;
                            case CreateReportStepsEnum.CreationApproveWithPhoto:
                                if (message.Text.ToLower().Equals("так"))
                                {
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id,
                                            "Будь ласка, опишіть суть вашої скарги",
                                            replyMarkup: new ReplyKeyboardHide());
                                    incident.CreationStep = CreateReportStepsEnum.EnterDescription;
                                    db.SaveChanges();
                                    break;
                                }
                                if (message.Text.ToLower().Equals("ні"))
                                {
                                    incident.CreationStep = CreateReportStepsEnum.Canceled;
                                    user.CreatingIncidentId = null;
                                    db.SaveChanges();
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id, "Ваш звіт скасовано",
                                            replyMarkup: new ReplyKeyboardHide());
                                    break;
                                }
                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id, "Створити звіт і прив`язати фото?",
                                        replyMarkup: GetYesNoKeyboard());
                                break;
                            case CreateReportStepsEnum.EnterDescription:
                                incident.CreationStep = CreateReportStepsEnum.GetLocation;
                                incident.Description = message.Text;
                                db.SaveChanges();
                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id, "Де стався цей інцидент?",
                                        replyMarkup:
                                            new ReplyKeyboardMarkup(new[]
                                            {new KeyboardButton("Відправити мітку") {RequestLocation = true}}));
                                break;
                            case CreateReportStepsEnum.EnterPhoto:
                                if (message.Text.ToLower().Equals("так"))
                                {
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id, "Будь ласка, завантажте фото",
                                            replyMarkup: new ReplyKeyboardHide());
                                    break;
                                }
                                if (message.Text.ToLower().Equals("ні"))
                                {
                                    incident.CreationStep = CreateReportStepsEnum.GetLocation;
                                    db.SaveChanges();
                                    await
                                        Bot.SendTextMessageAsync(message.Chat.Id,
                                            "Де стався цей інцидент?",
                                            replyMarkup:
                                                new ReplyKeyboardMarkup(new[]
                                                {new KeyboardButton("Відправити мітку") {RequestLocation = true}}));
                                    break;
                                }
                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id, "Ви бажаєте привя`язати фото?",
                                        replyMarkup: GetYesNoKeyboard());
                                break;
                            case CreateReportStepsEnum.GetType:
                                IncidentType type = IncidentType.Other;
                                switch (message.Text)
                                {
                                    case "Дорожній":
                                        type = IncidentType.Road;
                                        break;
                                    case "Муніципальний":
                                        type = IncidentType.Municipal;
                                        break;
                                    case "Інший":
                                        type = IncidentType.Other;
                                        break;
                                }
                                incident.Type = type;
                                incident.CreationStep = CreateReportStepsEnum.Created;

                                user = GetUserByChannelId(message.Chat.Id);
                                user.CreatingIncidentId = null;
                                db.SaveChanges();

                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id,
                                        $"Дякую! Я прийняв ваш звіт у роботу. Номер цього звіту {incident.Id}. За його виконанням ви можете слідкувати на сайті http://map.skargabot.com.ua/",
                                        replyMarkup: new ReplyKeyboardHide());
                                break;
                            case CreateReportStepsEnum.GetLocation:
                                await
                                    Bot.SendTextMessageAsync(message.Chat.Id, "Де стався цей інцидент?",
                                        replyMarkup:
                                            new ReplyKeyboardMarkup(new[]
                                            {new KeyboardButton("Відправити мітку") {RequestLocation = true}}));
                                break;

                        }
                        return;
                    }

                    if (message.Type == MessageType.PhotoMessage)
                    {
                        if (incident.CreationStep == CreateReportStepsEnum.EnterPhoto)
                        {

                            var file = await Bot.GetFileAsync(message.Photo.LastOrDefault()?.FileId);

                            var filename = file.FileId + "." + file.FilePath.Split('.').Last();
                            var virtualpath = "/bot/uploads/reports/" + filename;
                            var path = @".\uploads\reports\" + filename;
                            using (var saveImageStream = System.IO.File.Open(path, FileMode.Create))
                            {
                                await file.FileStream.CopyToAsync(saveImageStream);
                            }

                            db.IncidentPhotos.Add(new IncidentPhoto
                            {
                                ThumbnailUrl = virtualpath,
                                Url = virtualpath,
                                IncidentId = incidentId.Value
                            });
                            incident.CreationStep = CreateReportStepsEnum.GetLocation;
                            db.SaveChanges();

                            var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                                new KeyboardButton("Відправити мітку")
                                {
                                    RequestLocation = true
                                }
                            });
                            await
                                Bot.SendTextMessageAsync(message.Chat.Id, "Де стався цей інцидент?",
                                    replyMarkup: keyboard);
                        }
                        return;
                    }

                    if (message.Type == MessageType.LocationMessage)
                    {
                        if (incident.CreationStep == CreateReportStepsEnum.GetLocation)
                        {
                            var location = message.Location;
                            incident.Latitude = location.Latitude;
                            incident.Longitude = location.Longitude;
                            incident.CreationStep = CreateReportStepsEnum.GetType;
                            db.SaveChanges();

                            var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                                new KeyboardButton("Дорожній"), new KeyboardButton("Муніципальний"),
                                new KeyboardButton("Iнший")
                            });
                            await
                                Bot.SendTextMessageAsync(message.Chat.Id, "Яка категорія вашого звіту?",
                                    replyMarkup: keyboard);

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException?.Message);

                if (message != null)
                {
                    await
                        Bot.SendTextMessageAsync(message.Chat.Id, "Нажаль виникла помилка, попробуйте ще раз",
                            replyMarkup: new ReplyKeyboardHide());
                }

            }

        }

        private static Incident GetIncidentById(int id)
        {
            return db.Incidents.Find(id);
        }

        private static User GetUserByChannelId(long id)
        {
            //return db.ExternalUsers.FirstOrDefault(t => t.ChatId == id);
        }

        private static int? GetIncidentIdByChannelId(long id)
        {
            var user = GetUserByChannelId(id);
            return user == null ? null : user.CreatingIncidentId;
        }

        private static ReplyKeyboardMarkup GetYesNoKeyboard()
        {
            return new ReplyKeyboardMarkup(new[] { new KeyboardButton("Так"), new KeyboardButton("Ні") });
        }
    }

    /*class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient("295492480:AAEK203gLLNg0y_JUIML6NKM1jPrPZ_pXo8");

        static void Main(string[] args)
        {
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            var me = Bot.GetMeAsync().Result;

            Console.Title = me.Username;

            Bot.StartReceiving();
            Console.ReadLine();
            Bot.StopReceiving();
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            //Debugger.Break();
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received choosen inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            InlineQueryResult[] results = {
                new InlineQueryResultLocation
                {
                    Id = "1",
                    Latitude = 40.7058316f, // displayed result
                    Longitude = -74.2581888f,
                    Title = "New York",
                    InputMessageContent = new InputLocationMessageContent // message if result is selected
                    {
                        Latitude = 40.7058316f,
                        Longitude = -74.2581888f,
                    }
                },

                new InlineQueryResultLocation
                {
                    Id = "2",
                    Longitude = 52.507629f, // displayed result
                    Latitude = 13.1449577f,
                    Title = "Berlin",
                    InputMessageContent = new InputLocationMessageContent // message if result is selected
                    {
                        Longitude = 52.507629f,
                        Latitude = 13.1449577f
                    }
                }
            };

            await Bot.AnswerInlineQueryAsync(inlineQueryEventArgs.InlineQuery.Id, results, isPersonal: true, cacheTime: 0);
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.TextMessage) return;

            if (message.Text.StartsWith("/inline")) // send inline keyboard
            {
                await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                var keyboard = new InlineKeyboardMarkup(new[]
                {
                    new[] // first row
                    {
                        new InlineKeyboardButton("1.1"),
                        new InlineKeyboardButton("1.2"),
                    },
                    new[] // second row
                    {
                        new InlineKeyboardButton("2.1"),
                        new InlineKeyboardButton("2.2"),
                    }
                });

                await Task.Delay(500); // simulate longer running task

                await Bot.SendTextMessageAsync(message.Chat.Id, "Choose",
                    replyMarkup: keyboard);
            }
            else if (message.Text.StartsWith("/keyboard")) // send custom keyboard
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("1.1"),
                        new KeyboardButton("1.2"),
                    },
                    new [] // last row
                    {
                        new KeyboardButton("2.1"),
                        new KeyboardButton("2.2"),
                    }
                });

                await Bot.SendTextMessageAsync(message.Chat.Id, "Choose",
                    replyMarkup: keyboard);
            }
            else if (message.Text.StartsWith("/photo")) // send a photo
            {
                await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                const string file = @"<FilePath>";

                var fileName = file.Split('\\').Last();

                using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var fts = new FileToSend(fileName, fileStream);

                    await Bot.SendPhotoAsync(message.Chat.Id, fts, "Nice Picture");
                }
            }
            else if (message.Text.StartsWith("/request")) // request location or contact
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new KeyboardButton("Location")
                    {
                        RequestLocation = true
                    },
                    new KeyboardButton("Contact")
                    {
                        RequestContact = true
                    },
                });

                await Bot.SendTextMessageAsync(message.Chat.Id, "Who or Where are you?", replyMarkup: keyboard);
            }
            else
            {
                var usage = @"Usage:
/inline   - send inline keyboard
/keyboard - send custom keyboard
/photo    - send a photo
/request  - request location or contact
";

                await Bot.SendTextMessageAsync(message.Chat.Id, usage,
                    replyMarkup: new ReplyKeyboardHide());
            }
        }

        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            await Bot.AnswerCallbackQueryAsync(callbackQueryEventArgs.CallbackQuery.Id,
                $"Received {callbackQueryEventArgs.CallbackQuery.Data}");
        }
    }*/

