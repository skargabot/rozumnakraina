﻿using System.Web;
using System.Web.Mvc;

namespace SkargaBotApi
{
    public class TokenAuthorizeAttribute: AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var token = httpContext.Request.Headers["Authorization"];
            //TODO check token;
            return true;
        }
    }
}