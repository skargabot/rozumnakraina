﻿using SkargaBotApi.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web.Http;
using DataModel;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using DataModel.Models;
using SkargaBotApi.App_Start;
using Microsoft.Owin.Security.Cookies;

[assembly: OwinStartup(typeof(SkargaBotApi.Startup))]

namespace SkargaBotApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);

            CreateSuperAdmin();
        }
        public static string PublicClientId { get; private set; }
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public void ConfigureOAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(SkargaBotContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            //OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            //{
            //    AllowInsecureHttp = true,
            //    TokenEndpointPath = new PathString("/token"),
            //    AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
            //    Provider = new SimpleAuthorizationServerProvider()
            //};


            //// Token Generation
            //app.UseOAuthAuthorizationServer(OAuthServerOptions);
            //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            //app.UseCookieAuthentication(new CookieAuthenticationOptions());
            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/account/token"),
                Provider = new SimpleAuthorizationServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
        }
        private void CreateSuperAdmin()
        {
            SkargaBotContext context = new SkargaBotContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
  
            if (!roleManager.RoleExists("Адміністратор"))
            {
                var role1 = new IdentityRole();
                role1.Name = "Адміністратор";
                roleManager.Create(role1);

                var user = new ApplicationUser();
                user.UserName = "admin@rk.local";
                user.Email = "admin@rk.local";
                user.Name = "Головний Адмiнiстратор";

                string userPWD = "Admin123";

                var chkUser = UserManager.Create(user, userPWD);

                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Адміністратор");
                }
            }

            if (!roleManager.RoleExists("Диспетчер"))
            {
                roleManager.Create(new IdentityRole {Name = "Диспетчер"});
                roleManager.Create(new IdentityRole { Name = "Водій" });
                roleManager.Create(new IdentityRole { Name = "Користувач" });
                roleManager.Create(new IdentityRole { Name = "Майстер" });
            }
        }
    }
}
