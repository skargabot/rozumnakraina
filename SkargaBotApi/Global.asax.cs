﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Microsoft.Bot.Builder.Dialogs;
using SkargaBotApi.Bot;

namespace SkargaBotApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            var builder = new ContainerBuilder();
            builder.RegisterModule(new DefaultExceptionMessageOverrideModule());
            builder.Update(Conversation.Container);
        }
    }
}
