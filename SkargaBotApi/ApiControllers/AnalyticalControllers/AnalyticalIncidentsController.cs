﻿using DataModel.Models.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DataModel.ViewModels;
using DataModel.Repositories.Analytical;
using System.Web.Http.Description;
using System.Data;
using System.Data.Entity.Infrastructure;
/*
namespace SkargaBotApi.ApiControllers.AnalyticalControllers
{
    [Authorize, RoutePrefix("api/analytical/incidents")]
    public class AnalyticalIncidentsController : ApiController
    {
        private AnalyticalIncidentsRepository _rep;

        public AnalyticalIncidentsController()
        {
            _rep = new AnalyticalIncidentsRepository();
        }
        [HttpGet, AllowAnonymous, Route("list")]
        public IEnumerable<AnalyticalIncidentViewModel> List()
        {
            return _rep.GetAnalyticalIncidentsViewModelsList();
        }

        [ResponseType(typeof(void))]
        [HttpPost, AllowAnonymous, Route("connections")]
        public IHttpActionResult CreateConnection(IncidentsConnection connection)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _rep.CreateIncidentsConnection(connection);
            }
            catch (DbUpdateException)
            {
                if (_rep.IncidentsConnectionExists(connection.Incident1Id, connection.Incident2Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        [HttpGet, AllowAnonymous, Route("{incidentId}/history"), ResponseType(typeof(List<IncidentHistoryResult>))]
        public IHttpActionResult GetIncidentHistory(int incidentId)
        {
            AnalyticalIncident analyticalIncident = _rep.FindAnalyticalIncident(incidentId);
            if (analyticalIncident == null)
            {
                return NotFound();
            }

            return Ok(_rep.GetAnalyticalIncidentHistory(analyticalIncident).Select(i => new IncidentHistoryResult
            {
                Date = i.Date,
                Event = i.Event
            }));
        }
    }
}*/