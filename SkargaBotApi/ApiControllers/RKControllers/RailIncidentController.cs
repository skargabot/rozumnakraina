﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DataModel.ApiModels.RK;
using Microsoft.AspNet.Identity;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    [RoutePrefix("api/rk/railincidents")]
    public class RailIncidentController : ApiController
    {
        private DataModel.Repositories.RK.RailIncidentRepository _rep;
        public RailIncidentController()
        {
            _rep = new DataModel.Repositories.RK.RailIncidentRepository();
        }
        [HttpPost, Route("create")]
        public int Create(RailIncident apiIncident)//returns incident id, which is used for sending image
        {
            string userId = RequestContext.Principal.Identity.GetUserId();
            return _rep.Create(apiIncident, userId);
        }
        [HttpGet, Route("types")]
        public List<DataModel.Models.RozumnaKraina.RailIncidentType> GetIncidentTypes()
        {
            return _rep.GetIncidentTypes();
        }
        [HttpGet, Route("trains")]
        public List<DataModel.Models.RozumnaKraina.Train> GetTrains()
        {
            return _rep.GetTrains();
        }
    }
}