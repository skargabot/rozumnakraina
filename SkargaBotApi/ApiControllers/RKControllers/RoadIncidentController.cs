﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using DataModel.Repositories.RK;

using Microsoft.AspNet.Identity;

using SkargaBotApi.ApiModels;

using RoadIncident = DataModel.ApiModels.RK.RoadIncident;
using System;
using DataModel;
using DataModel.ApiModels.RK;
using DataModel.Enums;
using DataModel.Extentions;
using Geocoding.Google;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/rk/roadincidents")]
    public class RoadIncidentController : ApiController
    {
        private readonly RoadIncidentRepository _roadIncidentRepository;
        private readonly DemoIncidentRepository _demoRepository;

        /// <summary>
        /// Initializes a new instance of <see cref="RoadIncidentController"/> class.
        /// </summary>
        public RoadIncidentController()
        {
            _roadIncidentRepository = new RoadIncidentRepository();
            _demoRepository = new DemoIncidentRepository();
        }


        [HttpGet, Route("states")]
        public IEnumerable<object> GetStates()
        {
            var values = Enum.GetValues(typeof(IncidentStatePublic));
            foreach (IncidentStatePublic val in values)
            {
                yield return new {Id = val, Name = val.GetDisplayName()};
            }
        }

        [HttpGet, Route("incident/{id}")]
        public object GetIncedent(int id)
        {
            var incidents = _demoRepository.GetIncidents();
            var result = incidents.Select(_ => new
            {
                _.Id,
                _.Latitude,
                _.Longitude,
                _.CraetedAt,
                _.Description,
                _.UserId,
                _.GeocodedAddress,
                Route = new
                {
                    _.TransportRoute.Id,
                    _.TransportRoute.Name,
                    _.TransportRoute.StopStation,
                    _.TransportRoute.StartStation,
                    Type = new
                    {
                        _.TransportRoute.Type.Id,
                        _.TransportRoute.Type.Name,
                    }
                },
                Messages = _.Messages.OrderByDescending(m => m.PostDate).Select(m => new
                {
                    m.Id,
                    m.Text,
                    m.PostDate,
                    From = _demoRepository.GetUserRole(m.SenderId),
                    Images = m.Images.Select(i => new
                    {
                        i.Id,
                        i.ContentType
                    })
                }),
                State = new
                {
                    Id = _.State,
                    Name = _.State.GetDisplayName()
                },
                Images = _.Images.Select(i => new
                {
                    i.Id,
                    i.ContentType,
                }),
                Types = _.Relations.Select(r => new
                {
                    r.IncidentType.Id,
                    r.IncidentType.Name
                }).Concat( !string.IsNullOrEmpty(_.CustomIncidentType) ? new List<object> {new {Name = _.CustomIncidentType } } : new List<object>()),
                NearestIncidents = GetChildIncidentsByArea(_).Select(c => new
                {
                    c.Id,
                    c.GeocodedAddress,
                    Image = c.Images.Select(i => new
                    {
                        i.Id,
                        i.ContentType,
                    }).FirstOrDefault()
                }),
                SimilarIncidents = GetChidlIncidentsByType(_).Select(c => new
                {
                    c.Id,
                    Types = c.Relations.Select(r => new
                    {
                        r.IncidentType.Id,
                        r.IncidentType.Name
                    }),
                    Image = c.Images.Select(i => new
                    {
                        i.Id,
                        i.ContentType,
                    }).FirstOrDefault()
                })
            }).FirstOrDefault(_ => _.Id == id);

            return result;
        }

        [HttpGet, Route("myincidents")]
        public IEnumerable<object> GetMyIncedent()
        {
            var incidents = _demoRepository.GetIncidents();
            var result = incidents.Select(_ => new
            {
                _.Id,
                _.Latitude,
                _.Longitude,
                _.CraetedAt,
                _.Description,
                _.UserId,
                Route = new
                {
                    _.TransportRoute.Id,
                    _.TransportRoute.Name,
                    _.TransportRoute.StopStation,
                    _.TransportRoute.StartStation
                },
                State = new
                {
                    Id = _.State,
                    Name = _.State.GetDisplayName()
                },
                Images = _.Images.Select(i => new
                {
                    i.Id,
                    i.ContentType,
                }),
                Types = _.Relations.Select(r => new
                {
                    r.IncidentType.Id,
                    r.IncidentType.Name
                })
            }).Where(_ => _.UserId == User.Identity.GetUserId());

            return result;
        }


        [HttpPost, Route("incidents")]
        public IEnumerable<object> GetIncedents(FilterModel filterModel)
        {
            var incidents = _demoRepository.GetIncidents();

            if (RequestContext.Principal.IsInRole("Водій"))
            {
                incidents = incidents.Where(_ => _.UserId == RequestContext.Principal.Identity.GetUserId());
            }

            if (filterModel != null)
            {
                if (filterModel.Date != null)
                {
                    incidents = incidents.Where(_ => _.CraetedAt.Date == filterModel.Date.Value.Date);
                }

                if (filterModel.State != null)
                {
                    incidents = incidents.Where(_ => _.State == filterModel.State);
                }

                if (filterModel.IncidentTypesIds?.Length > 0)
                {
                    incidents = incidents.Where(_ => _.Relations.Count > 0 &&
                        _.Relations.Select(r => r.IncidentTypeId).Any(i => filterModel.IncidentTypesIds.Contains(i)));
                }

                if (filterModel.TransportTypeId != null)
                {
                    incidents = incidents.Where(_ => _.TransportTypeId == filterModel.TransportTypeId);
                }

                if (filterModel.RouteId != null)
                {
                    incidents = incidents.Where(_ => _.TransportRouteId == filterModel.RouteId);
                }

                if (filterModel.WithPhoto)
                {
                    incidents = incidents.Where(_ => _.Images.Count > 0);
                }

                if (filterModel.SortById)
                {
                    incidents = filterModel.SortAsc
                        ? incidents.OrderBy(_ => _.Id)
                        : incidents.OrderByDescending(_ => _.Id);
                }
                else
                {
                    incidents = filterModel.SortAsc
                        ? incidents.OrderBy(_ => _.CraetedAt)
                        : incidents.OrderByDescending(_ => _.CraetedAt);
                }
            }

            var result = incidents.Select(_ => new
            {
                _.Id,
                _.Latitude,
                _.Longitude,
                _.CraetedAt,
                _.Description,
                _.GeocodedAddress,
                Route = new
                {
                    _.TransportRoute.Id,
                    _.TransportRoute.Name,
                    _.TransportRoute.StopStation,
                    _.TransportRoute.StartStation,
                    Type = new
                    {
                        _.TransportRoute.Type.Id,
                        _.TransportRoute.Type.Name,
                    }
                },
                State = new
                {
                    Id = _.State,
                    Name = _.State.GetDisplayName()
                },
                Images = _.Images.Select(i => new
                {
                    i.Id,
                    i.ContentType,
                }),
                Types = _.Relations.Select(r => new
                {
                    r.IncidentType.Id,
                    r.IncidentType.Name
                }).Concat(!string.IsNullOrEmpty(_.CustomIncidentType) ? new List<object> { new { Name = _.CustomIncidentType } } : new List<object>())
            });

            return result;
        }

        /// <summary>
        /// Gets the list of possible incindent types.
        /// </summary>
        [HttpGet, Route("types")]
        public IEnumerable<object> GetIncedentTypes()
        {
            // Retrive data from repository.
            var types = _demoRepository.GetIncidentTypes();

            if (RequestContext.Principal.IsInRole("Водій"))
            {
                types = types.Where(_ => _.ForDrivers);
            }
            else
            {
                types = types.Where(_ => !_.ForDrivers);
            }


            // Convert data from reporsitory to dto.
            var typeDtos = types.Select(x => new {Id = x.Id, Name = x.Name});

            // Return list of dtos.
            return typeDtos;

            //// While all stuff above not implemented
            //// return list of mocked data:
            //return new List<object>
            //{
            //    new { Id = 1, Name = "зламані сидіння" },
            //    new { Id = 2, Name = "хамство водія" },
            //    new { Id = 3, Name = "хамство кондуктора" },
            //    new { Id = 4, Name = "занадто швидка їзда" }
            //};
        }

        /// <summary>
        /// Gets the list of avaliable transport types (like tram, bus etc).
        /// </summary>
        [HttpGet, Route("vehicle-types")]
        public IEnumerable<object> GetTransportTypes()
        {
            // Retrive data from repository.
            var types = _demoRepository.GetTransportTypes();

            // Convert data from reporsitory to dto.
            var typeDtos = types.Select(x => new {Id = x.Id, Name = x.Name});

            // Return list of dtos.
            return typeDtos;

            //// While all stuff above not implemented
            //// return list of mocked data:

            //var t1 = new TempTransportTypeDto { Id = 1, Name = "Трамвай" };
            //var t2 = new TempTransportTypeDto { Id = 2, Name = "Тролейбус" };
            //return new List<TempTransportTypeDto> { t1, t2};
        }

        /// <summary>
        /// Gets the list of registered vehicles with links to related type and route.
        /// </summary>
        [HttpGet, Route("vehicles")]
        public IEnumerable<object> GetTransports()
        {
            // Retrive data from repository.
            var transports = _demoRepository.GetTransports();

            // Convert data from reporsitory to dto.
            var transportDtos = transports.Select(x => new
            {
                Id = x.Id,
                Code = x.Code,
                TypeId = x.TypeId,
                TypeName = x.Type.Name,
                RouteId = x.RouteId,
                RouteName = x.Route != null ? $"{x.Route.Name} ({x.Route.StartStation} - {x.Route.StopStation})" : ""
            });

            // Return list of dtos.
            return transportDtos;

            //// While all stuff above not implemented
            //// return list of mocked data:

            //// Create dtos for trams.
            //var t = new List<TempTransportDto>();
            //var r = new Random();
            //for (int id = 1, code = 1001; code <= 1187; id++, code++)
            //    t.Add(new TempTransportDto { Id = id, Code = code.ToString(), TypeId = 1, RouteId = r.Next(1, 6) });

            //// Create dtos for trolleybuses.
            //for (int id = 1, code = 100; code <= 610; id++, code++)
            //    t.Add(new TempTransportDto { Id = id, Code = code.ToString(), TypeId = 2, RouteId = r.Next(1, 6) });

            //return t;
        }

        /// <summary>
        /// Gets the list of registered routes.
        /// </summary>
        [HttpGet, Route("routes")]
        public IEnumerable<object> GetRoutes()
        {
            // Retrive data from repository.
            var routes = _demoRepository.GetTransportRoutes();

            // Convert data from reporsitory to dto.
            var routeDtos = routes.Select(x =>
                new {Id = x.Id, Name = $"{x.Name} ({x.StartStation} - {x.StopStation})", TypeId = x.TypeId});

            // Return list of dtos.
            return routeDtos;

            //// While all stuff above not implemented
            //// return list of mocked data:
            //return new List<TempRouteDto>
            //{
            //    new TempRouteDto { Id = 1, Name = "Маршрут 1" },
            //    new TempRouteDto { Id = 2, Name = "Маршрут 2" },
            //    new TempRouteDto { Id = 3, Name = "Маршрут 3" },
            //    new TempRouteDto { Id = 4, Name = "Маршрут 4" },
            //    new TempRouteDto { Id = 5, Name = "Маршрут 5" }
            //};
        }

        /// <summary>
        /// Registers new incident.
        /// </summary>
        [HttpPost, Route("create")]
        public async Task<object> RegisterIncident(DemoIncidentDto incidentDto)
        {
            var geocoder = new GoogleGeocoder("AIzaSyBIn9xNcphoBCZRWIDNNPUYtAiP3qU07_8");
            geocoder.Language = "ru";
            var addresses = await geocoder.ReverseGeocodeAsync(incidentDto.Latitude, incidentDto.Longitude);
            var street = addresses.First()[GoogleAddressType.Route].ShortName;

            incidentDto.Address = street;

            incidentDto.UserId = User.Identity.GetUserId();
            return (await _demoRepository.CreateIncident(incidentDto))?.Id;
        }

        private class TempTransportTypeDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        private class TempTransportDto
        {
            public int Id { get; set; }
            public string Code { get; set; }

            public int TypeId { get; set; }
            public int RouteId { get; set; }
        }

        private class TempRouteDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        /*[HttpPost, Route("create")]
        public int Create(RoadIncident apiIncident) 
        {
            // Returns incident id, which is used for sending image.

            string userId = RequestContext.Principal.Identity.GetUserId();
            return _roadIncidentRepository.Create(apiIncident, userId);
        }

        [HttpGet, Route("types")]
        public List<ItemModel> GetIncidentTypes()
        {
            var model = _roadIncidentRepository.GetIncidentTypes().Select(_ => new ItemModel()
            {
                Id = _.Id,
                Name = _.Name
            }).ToList();
            return model;
        }

        [HttpGet, Route("get")]
        public List<DataModel.Models.RozumnaKraina.RoadIncident> GetIncidents()
        {
            return _roadIncidentRepository.GetIncidents();
        }

        [HttpGet, Route("oblasts")]
        public List<ItemModel> GetOblasts()
        {
            var model = _roadIncidentRepository.GetOblasts().Select(_ => new ItemModel()
            {
                Id = _.Id,
                Name = _.Name
            }).ToList();
            return model;
        }

        [HttpGet, Route("roads")]
        public List<ItemModel> GetRoads()
        {
            var model = _roadIncidentRepository.GetRoads().Select(_ => new ItemModel()
            {
                Id = _.Id,
                Name = _.Name
            }).ToList();
            return model;
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("stat")]
        public object GetStat()
        {
            var userId = RequestContext.Principal.Identity.GetUserId();
            var incidents = _demoRepository.GetIncidents().ToList(); //_roadIncidentRepository.GetIncidents();
            var done = incidents.Where(_ => _.State == IncidentStatePublic.Closed);

            var byTypes = _demoRepository.GetIncidentTypes().Select(_ => new
            {
                Item = _.Name,
                Value = _.Relations.Count
            }).Where(_ => _.Value > 0).OrderByDescending(_ => _.Value);

            var byRoutes = _demoRepository.GetTransportRoutes().Select(_ => new
            {
                Item = $"{_.Name} ({_.Type.Name})",
                Value = incidents.Count(i => i.TransportRouteId == _.Id)
            }).Where(_ => _.Value > 0).OrderByDescending(_ => _.Value);

            var byRouteTypes = _demoRepository.GetTransportTypes().Select(_ => new
            {
                Item = _.Name,
                Value = incidents.Count(i => i.TransportTypeId == _.Id)
            }).Where(_ => _.Value > 0).OrderByDescending(_ => _.Value);

            var reportsDone = new List<dynamic>();

            reportsDone.Add(new
            {
                Item = "За типами інциденту",
                Value = incidents.Count(_ => _.State == IncidentStatePublic.Closed && _.Relations.Any())
            });

            reportsDone.Add(new
            {
                Item = "За типом транспортного засобу",
                Value = incidents.Count(_ => _.State == IncidentStatePublic.Closed)
            });

            reportsDone.Add(new
            {
                Item = "За номером маршруту",
                Value = incidents.Count(_ => _.State == IncidentStatePublic.Closed)
            });

            var model = new
            {
                Reports = incidents.Count(),
                ReportsInProgress = incidents.Count(),
                ReportrsDone = done.Count(),
                ByTypes = byTypes,
                ByRoutes = byRoutes,
                ByTrasportTypes = byRouteTypes,
                Closed = reportsDone.Where(_ => _.Value > 0).OrderByDescending(_ => _.Value)
        };

            return model;
        }

        private static double ClaculateLatByAreaDistance(double pointLat, int offset)
        {
            //Earth’s radius, sphere
            double r = 6378137;
            // Coordinate offsets in radians
            var dLat = offset / r;
            //OffsetPosition
            var latO = pointLat + dLat * 180 / Math.PI;
            return latO;
        }

        private static double ClaculateLonByAreaDistance(double pointLat, double pointLon, int offset)
        {
            //Earth’s radius, sphere
            double r = 6378137;
            // Coordinate offsets in radians
            var dLon = offset / (r * Math.Cos(Math.PI * pointLat / 180));
            //OffsetPosition
            var lonO = pointLon + dLon * 180 / Math.PI;
            return lonO;
        }

        private IEnumerable<DemoIncident> GetChildIncidentsByArea(DemoIncident source)
        {
            var distanceArea = 1000;

            var latOffsetPos = ClaculateLatByAreaDistance(source.Latitude, distanceArea);
            var lonOffsetPos = ClaculateLonByAreaDistance(source.Latitude, source.Longitude, distanceArea);
            var latOffsetNeg = ClaculateLatByAreaDistance(source.Latitude, -distanceArea);
            var lonOffsetNeg = ClaculateLonByAreaDistance(source.Latitude, source.Longitude, -distanceArea);

            var incidents = _demoRepository.GetIncidents().Where(p => p.Id != source.Id &&
                p.Latitude <= latOffsetPos && p.Longitude <= lonOffsetPos && p.Latitude >= latOffsetNeg &&
                p.Longitude >= lonOffsetNeg).OrderByDescending(_ => _.CraetedAt).Take(10);

            return incidents;
        }

        private IEnumerable<DemoIncident> GetChidlIncidentsByType(DemoIncident source)
        {
            var incidentsTypes = source.Relations.Select(_ => _.IncidentTypeId);
            var incidents = _demoRepository.GetIncidents()
                .Where(_ => _.Id != source.Id && _.Relations.Any(r => incidentsTypes.Contains(r.IncidentTypeId))).OrderByDescending(_ => _.CraetedAt).Take(10);

            return incidents;
        }
    }
}