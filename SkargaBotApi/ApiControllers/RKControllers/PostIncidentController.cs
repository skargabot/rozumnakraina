﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    public class PostIncidentController : ApiController
    {
        private DataModel.Repositories.RK.PostIncidentRepository _rep;
        public PostIncidentController()
        {
            _rep = new DataModel.Repositories.RK.PostIncidentRepository();
        }
        [HttpPost, Route("create")]
        public int Create(DataModel.ApiModels.RK.PostIncident apiIncident)//returns incident id, which is used for sending image
        { 
            string userId = RequestContext.Principal.Identity.GetUserId();
            return _rep.Create(apiIncident, userId);
        }
        [HttpGet, Route("types")]
        public List<DataModel.Models.RozumnaKraina.PostIncidentType> GetIncidentTypes()
        {
            return _rep.GetIncidentTypes();
        }
    }
}