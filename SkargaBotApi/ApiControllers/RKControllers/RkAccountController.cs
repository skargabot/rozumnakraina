﻿using DataModel.Repositories.RK;
using DataModel.ViewModels.RK;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DataModel;
using DataModel.Models;
using DataModel.Models.RozumnaKraina;
using DataModel.Repositories.Analytical;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using SkargaBotApi.Providers;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    [RoutePrefix("api/rk/account")]
    public class RkAccountController : ApiController
    {
        private AccountRepository _repo = null;

        public RkAccountController()
        {
            _repo = new AccountRepository();
        }
        
        [Route("register")]
        public async Task<IHttpActionResult> Create(UserViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _repo.RegisterUser(userModel);

            var errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        [Route("facebook"), HttpPost]
        public async Task<IHttpActionResult> Facebook(dynamic data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var facebookUserManager = new UserManager<FacebookUser>(new UserStore<FacebookUser>(new SkargaBotContext()));

            using (var client = new HttpClient())
            {
                var url = "https://graph.facebook.com/v2.8/me?fields=id%2Cname%2Cemail" +
                          "&access_token=" + data.fbToken.ToString();
                var response = await client.GetStringAsync(url);
                JObject fbUser = JObject.Parse(response);

                if (fbUser?["id"] == null)
                    return null;

                var newUser = new FacebookUser
                {
                    Email = fbUser["email"]?.Value<string>(),
                    Name = fbUser["name"]?.Value<string>(),
                    UserName = fbUser["id"]?.Value<string>()
                };

                var user = facebookUserManager.FindByName(newUser.UserName);
                if (user == null)
                {
                    var result = facebookUserManager.Create(newUser);
                    user = facebookUserManager.FindById(newUser.Id);
                    facebookUserManager.AddToRole(user.Id, "Користувач");
                }

                var oAuthIdentity = await facebookUserManager.CreateIdentityAsync(user, OAuthDefaults.AuthenticationType);
                oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
                oAuthIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
                var ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
                ticket.Properties.IssuedUtc = DateTime.UtcNow;
                ticket.Properties.ExpiresUtc = DateTime.UtcNow.Add(TimeSpan.FromDays(7));

                var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);
                Request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);

                var token = new JObject(
                    new JProperty("userName", user.UserName),
                    new JProperty("name", user.Name),
                    new JProperty("surname", user.Surname),
                    new JProperty("userId", user.Id),
                    new JProperty("access_token", accessToken),
                    new JProperty("token_type", "bearer"),
                    new JProperty("expires_in", TimeSpan.FromDays(365).TotalSeconds.ToString()),
                    new JProperty("issued", DateTime.UtcNow.ToString("ddd, dd MMM yyyy HH':'mm':'ss 'GMT'")),
                    new JProperty("expires", DateTime.UtcNow.Add(TimeSpan.FromDays(7)).ToString("ddd, dd MMM yyyy HH:mm:ss 'GMT'"))
                );

                return Ok(token);
            }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}