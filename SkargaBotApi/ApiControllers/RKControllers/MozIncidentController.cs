﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    [RoutePrefix("api/rk/mozincidents")]
    public class MozIncidentController : ApiController
    {
        private DataModel.Repositories.RK.MozIncidentRepository _rep;
        public MozIncidentController()
        {
            _rep = new DataModel.Repositories.RK.MozIncidentRepository();
        }
        [HttpPost, Route("create")]
        public int Create(DataModel.ApiModels.RK.MozIncident apiIncident)//returns incident id, which is used for sending image
        {
            string userId = RequestContext.Principal.Identity.GetUserId();
            return _rep.Create(apiIncident, userId);
        }
        [HttpGet, Route("types")]
        public List<DataModel.Models.RozumnaKraina.MozIncidentType> GetIncidentTypes()
        {
            return _rep.GetIncidentTypes();
        }
        [HttpGet, Route("get")]
        public List<DataModel.Models.RozumnaKraina.MozIncident> GetIncidents()
        {
            return _rep.GetIncidents();
        }
        [HttpGet, Route("oblasts")]
        public List<DataModel.Models.RozumnaKraina.Oblast> GetOblasts()
        {
            return _rep.GetOblasts();
        }
        [HttpGet, Route("hospitals")]
        public List<DataModel.Models.RozumnaKraina.Hospital> GetHospitals()
        {
            return _rep.GetHospitals();
        }
    }
}