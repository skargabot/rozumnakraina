﻿using DataModel.Repositories.RK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    [RoutePrefix("api/rk/iinfo")]
    public class ImportInfoController : ApiController
    {
        private ImportInfoRepository _rep;
        private string path = "C:\\work\\rozumnakraina\\";
        public ImportInfoController()
        {
            _rep = new ImportInfoRepository();
        }
        [HttpGet, Route("oblasts")]
        public void ImportOblasts()
        {
            _rep.ImportOblasts(new StreamReader(path + "DataModel\\Info\\RK\\Oblasts.txt"));
        }
        [HttpGet, Route("roads")]
        public void ImportRoads()
        {
            _rep.ImportRoads(new StreamReader(path + "DataModel\\Info\\RK\\Roads.txt"));
        }
        [HttpGet, Route("roadinctypes")]
        public void ImportRoadIncidentTypes()
        {
            _rep.ImportRoadIncidentTypes(new StreamReader(path + "DataModel\\Info\\RK\\RoadIncidentTypes.txt"));
        }
        [HttpGet, Route("railinctypes")]
        public void ImportRailIncidentTypes()
        {
            _rep.ImportTrainIncidentTypes(new StreamReader(path + "DataModel\\Info\\RK\\RailIncidentTypes.txt"));
        }
        [HttpGet, Route("postinctypes")]
        public void ImportPostIncidentTypes()
        {
            _rep.ImportPostIncidentTypes(new StreamReader(path + "DataModel\\Info\\RK\\PostIncidentTypes.txt"));
        }
        [HttpGet, Route("mozinctypes")]
        public void ImportMozIncidentTypes()
        {
            _rep.ImportMozIncidentTypes(new StreamReader(path + "DataModel\\Info\\RK\\MozIncidentTypes.txt"));
        }
        [HttpGet, Route("hospitals")]
        public void ImportHospitals()
        {
            _rep.ImportHospitals(new StreamReader(path + "DataModel\\Info\\RK\\Hopitals.txt"));
        }
        [HttpGet, Route("trains")]
        public void ImportTrains()
        {
            _rep.ImportTrains(new StreamReader(path + "DataModel\\Info\\RK\\Trains.txt"));
        }
    }
}
