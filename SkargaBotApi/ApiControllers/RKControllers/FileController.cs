﻿using DataModel.Repositories.RK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;

namespace SkargaBotApi.ApiControllers.RKControllers
{
    [System.Web.Http.RoutePrefix("api/rk/files")]
    public class FileController : ApiController
    {
        private FileRepository _rep;
        private string path = "C:\\work\\rozumnakraina\\";
        public FileController()
        {
            _rep = new FileRepository();
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("post/{incidentId}")]
        public async Task<HttpResponseMessage> PostFile(int incidentId)
        {
            HttpRequestMessage request = Request;
            var files = HttpContext.Current.Request.Files;
            //if (!request.Content.IsMimeMultipartContent())
            //{
            //    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            //}

            //string root = System.Web.HttpContext.Current.Server.MapPath("");
            //var provider = new MultipartFileStreamProvider(root);

            //await request.Content.ReadAsMultipartAsync(provider);

            foreach (string file in files)
            {
                var id = Guid.NewGuid();
                var postedFile = HttpContext.Current.Request.Files[file];
                var relative = $"/App_Data/Uploads/{id}";
                var filePath = HttpContext.Current.Server.MapPath($"~{relative}");
                postedFile.SaveAs(filePath);

                _rep.SaveFileInfo(incidentId, null, id.ToString(), postedFile.ContentType);
            }
        
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [System.Web.Http.HttpPost, System.Web.Http.Route("post/message/{msgId}")]
        public async Task<HttpResponseMessage> PostImageToMessage(int msgId)
        {
            var files = HttpContext.Current.Request.Files;

            foreach (string file in files)
            {
                var id = Guid.NewGuid();
                var postedFile = HttpContext.Current.Request.Files[file];
                var relative = $"/App_Data/Uploads/{id}";
                var filePath = HttpContext.Current.Server.MapPath($"~{relative}");
                postedFile.SaveAs(filePath);

                _rep.SaveFileInfo(null, msgId, id.ToString(), postedFile.ContentType);
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("getfile")]
        public HttpResponseMessage GetFile(string id)
        {
            string root = HttpContext.Current.Server.MapPath("~/App_Data/Uploads/"+id);
            Stream iconStream = new FileStream(root, FileMode.Open);
            if (iconStream == null)
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("Image not found")
                };
            }

            var response = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StreamContent(iconStream),
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");

            return response;
        }

    }
}
