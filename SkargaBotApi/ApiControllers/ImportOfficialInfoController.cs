﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using DataModel;
using DataModel.Enums;
using DataModel.Models;
using SkargaBotApi.ApiModels;
using System;
using DataModel.ViewModels;
using System.Web.Http.Description;
using DataModel.Extentions;
using DataModel.Repositories;
using System.IO;

namespace SkargaBotApi.ApiControllers
{
    [RoutePrefix("api/oinfo")]
    public class ImportOfficialInfoController : ApiController
    {
        private ImportOfficialInfoRepository _rep;
        public ImportOfficialInfoController()
        {
            _rep = new ImportOfficialInfoRepository();
        }
        [HttpGet, Route("streets")]
        public void ImportStreets()
        {
            _rep.ImportStreets(new StreamReader("c:\\st\\st.txt"));
        }
        [HttpGet, Route("cars")]
        public void ImportTransportRoutes()
        {
            _rep.ImportRoutes(new StreamReader("c:\\st\\car.txt"));
        }
    }
}