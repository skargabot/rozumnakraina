﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using DataModel;
using DataModel.Enums;
using DataModel.Models;
using SkargaBotApi.ApiModels;
using System;
using DataModel.ViewModels;
using System.Web.Http.Description;
using DataModel.Extentions;
using DataModel.Repositories;

namespace SkargaBotApi.ApiControllers
{
    [RoutePrefix("api/incidents")]
    public class IncidentsController : ApiController
    {
        private IncidentsRepository _rep;
        public IncidentsController()
        {
            _rep = new IncidentsRepository();
        }
         
        /// <summary>
        /// Returns incidents list
        /// </summary>
        /// <param name="filter">Request filters</param>
        /// <returns>Incidents list</returns>
        [HttpGet, Route("list")]
        public IEnumerable<Incident> List([FromUri]IncidentFilter filter)
        {   
            var incidents = _rep.GetIncidents();

            if (filter != null)
            {
                if (filter.StarDate != null)
                {
                    incidents = incidents.Where(i => i.Date > filter.StarDate);
                }

                if (filter.EndDate != null)
                {
                    incidents = incidents.Where(i => i.Date < filter.EndDate);
                }

                if (filter.State?.Length > 0)
                {
                    incidents = incidents.Where(i => filter.State.Contains(i.State));
                }

                if (filter.Priority?.Length > 0)
                {
                    incidents = incidents.Where(i => filter.Priority.Contains(i.Priority));
                }

                /*if (filter.Type?.Length > 0)
                {
                    incidents = incidents.Where(i => filter.Type.Contains(i.Type));
                }*/
            }

            return incidents.ToList();
        }

        /// <summary>
        /// Get incedent by id
        /// </summary>
        /// <param name="id">Incident id</param>
        /// <returns>Incident</returns>
        [HttpGet, Route("get")]
        public Incident Get(int id)
        {
            return _rep.GetIncidentById(id);
        }

        /// <summary>
        /// Change incedent state
        /// </summary>
        /// <param name="id">Incident id</param>
        /// <param name="state">New state</param>
        [HttpPost, Route("state")]
        public IHttpActionResult EditState(int id, int state)
        {
            Incident incident = _rep.FindIncident(id);
            if (incident == null)
            {
                return NotFound();
            }
            _rep.SetIncidentState(incident, (IncidentStatePublic)state);
            return Ok();
        }

        /// <summary>
        /// Get incidents statistics
        /// </summary>
        [HttpGet, Authorize(Roles = "SuperAdmin, Dispatcher")]
        public IncidentsStatsResult Stats()
        {
            return _rep.GetIncidentsStatistics();
        }

        /// <summary>
        /// Add link to incident mention
        /// </summary>
        /// <param name="IncidentId">Incident id</param>
        /// <param name="Url">Url to mention</param>
        [ResponseType(typeof(void))]
        [HttpPost, Authorize(Roles = "SuperAdmin, Dispatcher"), Route("mentions")]
        public IHttpActionResult AddIncidentMention(IncidentMention mention)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _rep.CreateIncidentMention(mention);

            return Ok();
        }

        /// <summary>
        /// Get incident history
        /// </summary>
        [HttpGet, Route("{incidentId}/history"), ResponseType(typeof(List<IncidentHistoryResult>))]
        public IHttpActionResult GetIncidentHistory(int incidentId)
        {
            Incident incident = _rep.FindIncident(incidentId);
            if (incident == null)
            {
                return NotFound();
            }

            return Ok(_rep.GetIncidentHistory(incident).Select(i => new IncidentHistoryResult
            {
                Date = i.Date,
                Event = i.Event
            }));
        }

        protected override void Dispose(bool disposing)
        {
            _rep.Dispose();
            base.Dispose(disposing);
        }
    }
}