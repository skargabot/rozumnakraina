﻿using System.Web.Http;
using DataModel.Repositories;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using DataModel.ViewModels;

namespace SkargaBotApi.ApiControllers
{
    public class AccountController: ApiController
    {
        //private SkargaBotContext db;

        private AuthRepository _repo = null;

        public AccountController()
        {
            _repo = new AuthRepository();
        }
        
        [Authorize(Roles = "SuperAdmin")]
        [Route("api/account/createadmin")]
        public async Task<IHttpActionResult> CreateAdmin(UserViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterUser(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        /*[Route("api/account/registateapp")]
        public async Task<IHttpActionResult> CreateExternalAppUser(ExternalAppUserViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterExternalAppUser(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }*/

        /*[Authorize(Roles = "SuperAdmin")]
        [Route("api/account/attachToCompany")]
        public async Task<IHttpActionResult> AttachUserToCompany(string userId, int companyId)
        {
            if (!_repo.AttachUserToCompany(userId, companyId))
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}