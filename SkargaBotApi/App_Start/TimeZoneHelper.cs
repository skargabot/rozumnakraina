﻿using System;

namespace SkargaBotApi
{
    public static class TimeZoneHelper
    {
        public static DateTime GetFleDateTime()
        {
            var fleDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(
                DateTime.UtcNow, "FLE Standard Time");
            return fleDateTime;
        }
    }
}