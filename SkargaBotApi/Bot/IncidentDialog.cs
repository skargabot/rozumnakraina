﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataModel.Enums;
using DataModel.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Linq;
using DataModel;
using DataModel.Helpers;

namespace SkargaBotApi.Bot
{
    [Serializable]
    public class IncidentDialog : IDialog<IncidentDialogModel>
    {
        private IncidentDialogModel _incident;
        private List<IncidentField> _customFields;
        private IncidentField _processedField;
        public IncidentDialog()
        {
            _incident = new IncidentDialogModel();
            _customFields = new SkargaBotContext().IncidentFields.ToList();
        }

        public async Task StartAsync(IDialogContext context)
        {
           // await context.PostAsync("Будь ласка, почніть с опису або фото");
            context.Wait(MessageRecived);
        }

        private async Task MessageRecived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var recivedActivity = await activity;

            if (!string.IsNullOrEmpty(recivedActivity.Text) && recivedActivity.Attachments == null &&
                recivedActivity.Entities == null)
            {
                StartWithText(context, recivedActivity);
            }
            else if (string.IsNullOrEmpty(recivedActivity.Text) && recivedActivity.Attachments?.Count > 0)
            {
                StartWithPhoto(context, recivedActivity);
            }
            else 
            {
                /*if (_incident.CreationStep == CreateReportStepsEnum.GetLocation)
                {
                    LocationRecivedAction(context, recivedActivity);
                }
                else
                {*/
                    //await context.PostAsync("Будьласка почніить с опису або фото");
                //}
            }
        }
        
        private void StartWithText(IDialogContext context, IMessageActivity activity)
        {
            _incident.Description = activity.Text;

            PromptDialog.Choice(
                context,
                ConfirmIncidentAction,
                new PromptOptions<string>("Створити звіт с таким описом?", "Будь ласка зробіть ваш вибір",
                    "Нажаль мы не змогли опрацювати ваш звіт, спробуйте ще раз", new List<string> { "Так", "Ні" }, 3,
                    new PromptStyler()));
        }

        private void StartWithPhoto(IDialogContext context, IMessageActivity activity)
        {
            _incident.IncidentPhotos = new List<IncidentPhotoDialogModel>();

            foreach (var attachment in activity.Attachments)
            {
                _incident.IncidentPhotos.Add(new IncidentPhotoDialogModel {ThumbnailUrl = attachment.ThumbnailUrl, Url = attachment.ContentUrl});
            }

            PromptDialog.Choice(
                context,
                AddDescriptionAction,
                new PromptOptions<string>("Створити звіт та прив`язати фото?", "Будь ласка зробіть ваш вибір",
                    "Нажаль мы не змогли опрацювати ваш звіт, спробуйте ще раз", new List<string> { "Так", "Ні" }, 3,
                    new PromptStyler()));
        }

        private async Task AddDescriptionAction(IDialogContext context, IAwaitable<string> result)
        {
            var resultString = await result;
            if (resultString.ToLower().Equals("так"))
            {
                PromptDialog.Text(context, DescriptionAddedAction, "Будь ласка, опишіть суть вашої скарги",
                "Сробуйте ще раз");
            }
            else if (resultString.ToLower().Equals("ні"))
            {
                await context.PostAsync("Ваш звіт скасовано");
            }
        }

        private async Task DescriptionAddedAction(IDialogContext context, IAwaitable<string> result)
        {
            var description = await result;
            _incident.Description = description;

            //_incident.CreationStep = CreateReportStepsEnum.GetType;

            PromptDialog.Choice(context,
                IncidentTypeSelectedAction, Enum.GetNames(typeof(IncidentType)),
                "Яка категорія вашого звіту?", "Будь ласка спробуйте ще раз", 3,
                PromptStyle.Auto, new List<string> { "Дорожній", "Муніципальній", "Іинший" });
        }

        private async Task ConfirmIncidentAction(IDialogContext context, IAwaitable<string> result)
        {
            var resultString = await result;
            if (resultString.ToLower().Equals("так"))
            {
                //_incident.CreationStep = CreateReportStepsEnum.CreationApproveWithDescription;

                PromptDialog.Choice(
                    context,
                    ConfirmPhotoAction,
                    new PromptOptions<string>("Бажаете завантажити фото?", "Будь ласка зробіть ваш вибір",
                        "Нажаль мы не змогли опрацювати ваш звіт, спробуйте ще раз", new List<string> { "Так", "Ні" }, 3,
                        new PromptStyler()));
            }
            else if (resultString.ToLower().Equals("ні"))
            {
                //_incident.CreationStep = CreateReportStepsEnum.Canceled;
                await context.PostAsync("Ваш звіт скасовано");
            }
        }

        private async Task ConfirmPhotoAction(IDialogContext context, IAwaitable<string> result)
        {
            var resultString = await result;
            if (resultString.ToLower().Equals("так"))
            {
                //_incident.CreationStep = CreateReportStepsEnum.EnterPhoto;

                PromptDialog.Attachment(context, IncidentPhotoRecivedAction, "Будь ласка, завантажте фото", null,
                    "Спробуйте ще раз");
            }
            else if (resultString.ToLower().Equals("ні"))
            {
                //_incident.CreationStep = CreateReportStepsEnum.GetType;

                PromptDialog.Choice(context,
                IncidentTypeSelectedAction, new List<string> { "Стан доріг", "Громадський транспорт" },
                "Яка категорія вашого звіту?", "Будь ласка спробуйте ще раз", 3,
                PromptStyle.Auto, new List<string> { "Стан доріг", "Громадський транспорт" });
            }

        }

        private async Task IncidentPhotoRecivedAction(IDialogContext context, IAwaitable<IEnumerable<Attachment>> result)
        {
            var attachments = await result;

            _incident.IncidentPhotos = new List<IncidentPhotoDialogModel>();

            foreach (var attachment in attachments)
            {
                _incident.IncidentPhotos.Add(new IncidentPhotoDialogModel { ThumbnailUrl = attachment.ThumbnailUrl, Url = attachment.ContentUrl });
            }

            //_incident.CreationStep = CreateReportStepsEnum.GetType;

            PromptDialog.Choice(context, 
                IncidentTypeSelectedAction, new List<string> { "Стан доріг", "Громадський транспорт" }, 
                "Будь ласка оберіть тип проблеми", "Будь ласка спробуйте ще раз", 3, 
                PromptStyle.Auto, new List<string> { "Стан доріг", "Громадський транспорт" });
        }

        private async Task IncidentTypeSelectedAction(IDialogContext context, IAwaitable<string> result)
        {
            var strType = await result;
            //_incident.Type = EnumHelper.GetEnumValueFromDescription<IncidentType>(strType);

            
            if (strType.ToLower().Equals("Стан доріг".ToLower()))
            {
                _incident.Street = new Street();
                PromptDialog.Choice(context,
                IncidentStreetTypeSelectedAction, new List<string> { "Вулиця", "Проспект", "Площа", "Провулок", "Проїзд", "Тупик", "Узвіз" },
                "Виберіть тип", "Будь ласка спробуйте ще раз", 3,
                PromptStyle.Auto, new List<string> { "Вулиця", "Проспект", "Площа", "Провулок", "Проїзд", "Тупик", "Узвіз" });
            }
            else 
            {
                _incident.TransportRoute = new TransportRoute();
                PromptDialog.Choice(context,
                IncidentRouteTypeSelectedAction, new List<string> { "Автобус", "Маршрутка", "Трамвай", "Тролейбус" },
                "Виберіть тип", "Будь ласка спробуйте ще раз", 3,
                PromptStyle.Auto, new List<string> { "Автобус", "Маршрутка", "Трамвай", "Тролейбус" });
            }
            //await context.PostAsync("Де стався цей інцидент? Відправте мені місцезнаходження");
        }

        private async Task IncidentStreetTypeSelectedAction(IDialogContext context, IAwaitable<string> result)
        {
            var strType = await result;
            _incident.Street.Type = strType;
            PromptDialog.Text(context, IsComplainAction, "Введіть назву",
                "Спробуйте ще раз");
        }

        private async Task IncidentRouteTypeSelectedAction(IDialogContext context, IAwaitable<string> result)
        {
            var strType = await result;
            _incident.TransportRoute.Type = strType;
            PromptDialog.Text(context, IsComplainAction, "Введіть номер маршруту",
                "Спробуйте ще раз");
        }

        private async Task IsComplainAction(IDialogContext context, IAwaitable<string> result)
        {
            var strType = await result;
            if(_incident.Street != null)
            {
                _incident.Street.Name = strType;
                using (var _ctx = new SkargaBotContext())
                {
                    var street = _ctx.Streets.FirstOrDefault(s => s.Name == _incident.Street.Name && s.Type == _incident.Street.Type);
                    if (street != null)
                    {
                        _incident.Street.Id = street.Id;
                        await context.PostAsync(_incident.Street.Type + " " + _incident.Street.Name + " - знайдено");
                    }
                }
            }
            else if (_incident.TransportRoute != null)
            {
                _incident.TransportRoute.Name = strType;
                using (var _ctx = new SkargaBotContext())
                {
                    var route = _ctx.TransportRoutes.FirstOrDefault(s => s.Name == _incident.TransportRoute.Name && s.Type == _incident.TransportRoute.Type);
                    if (route != null)
                    {
                        _incident.TransportRoute.Id = route.Id;
                        await context.PostAsync(_incident.TransportRoute.Type + " " + _incident.TransportRoute.Name + " - знайдено");
                    }
                }
            }
            PromptDialog.Choice(
                    context,
                    FlCallback,
                    new PromptOptions<string>("Виберіть тип звіту", "Будь ласка зробіть ваш вибір",
                        "Нажаль мы не змогли опрацювати ваш звіт, спробуйте ще раз", new List<string> { "Скарга", "Схвалення" }, 3,
                        new PromptStyler()));
        }

        private async Task FlCallback(IDialogContext context, IAwaitable<string> result)
        {
            var strType = await result;
            if(_processedField != null)
            {
                _incident.IncidentFieldValues.Add(new IncidentFieldValueDialogModel { IncidentFieldId = _processedField.Id, Value = strType });
            }

            if (!_customFields.Any())
            {
                ConfrimDialog(context);
                context.Done(this);
            }
                
            var q = _customFields.First().Question;
            _processedField = _customFields.First();
            _customFields.RemoveAt(0);
            PromptDialog.Text(context, FlCallback, q,
                "Спробуйте ще раз");
        }

        private void LocationRecivedAction(IDialogContext context, IMessageActivity activity)
        {
            var entity = activity.Entities[0];
            dynamic prop  = entity.Properties;

            _incident.Latitude = prop.geo.latitude ?? 0;
            _incident.Longitude = prop.geo.longitude ?? 0;

            ConfrimDialog(context);
        }

        private async void ConfrimDialog(IDialogContext context)
        {
            //try
            //{
                //_incident.CreationStep = CreateReportStepsEnum.Created;

                var incident = new Incident
                {
                    Description = _incident.Description,
                    Date = DateTime.Now,
                    Latitude = _incident.Latitude,
                    Longitude = _incident.Longitude,
                    IncidentPhotos = _incident.IncidentPhotos?.Select(p => new IncidentPhoto { ThumbnailUrl = p.ThumbnailUrl, Url = p.Url }).ToList(),
                    IncidentFieldsValues = _incident.IncidentFieldValues?.Select(v => new IncidentFieldValue { Value = v.Value, IncidentFieldId = v.IncidentFieldId}).ToList(),
                    StreetId = _incident.Street?.Id,
                    TransportRouteId = _incident.TransportRoute?.Id
                };

                using (var db = new SkargaBotContext())
                {
                    db.Incidents.Add(incident);
                    db.SaveChanges();
                }

                await context.PostAsync($"Дякую! Я прийняв ваш звіт у роботу. Номер цього звіту {incident.Id}.");
            //}
            /*catch (Exception e)
            {
                await context.PostAsync("Нажаль виникла помилка при опрацюванні вашого звіту, спробуйте ще раз, або зачекайте деякий час");
            }*/

        }
    }
}