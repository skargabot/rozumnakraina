﻿using System;
using System.Diagnostics;
using System.Resources;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Builder.Internals.Fibers;

namespace SkargaBotApi.Bot
{
    public class PostUnhandledExceptionToUserOverrideTask : IPostToBot
    {
        private readonly ResourceManager resources;
        private readonly IPostToBot inner;
        private readonly IBotToUser botToUser;
        private readonly TraceListener trace;

        public PostUnhandledExceptionToUserOverrideTask(IPostToBot inner, IBotToUser botToUser,
            ResourceManager resources, TraceListener trace)
        {
            SetField.NotNull(out this.inner, nameof(inner), inner);
            SetField.NotNull(out this.botToUser, nameof(botToUser), botToUser);
            SetField.NotNull(out this.resources, nameof(resources), resources);
            SetField.NotNull(out this.trace, nameof(trace), trace);
        }

        async Task IPostToBot.PostAsync<T>(T item, CancellationToken token)
        {
            try
            {
                await inner.PostAsync(item, token);
            }
            catch (Exception e)
            {
                try
                {
                    await botToUser.PostAsync("Нажаль виникла помилка при опрацюванні вашого звіту, спробуйте ще раз, або зачекайте деякий час", cancellationToken: token);
                }
                catch (Exception inner)
                {
                    trace.WriteLine(inner);
                }

                throw;
            }
        }
    }
}