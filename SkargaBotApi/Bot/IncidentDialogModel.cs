﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel.Models;

namespace SkargaBotApi.Bot
{
    [Serializable]
    public class IncidentDialogModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public IncidentType Type { get; set; }
        public List<IncidentPhotoDialogModel> IncidentPhotos { get; set; } 
        public Street Street { get; set; }
        public TransportRoute TransportRoute { get; set; }
        public List<IncidentFieldValueDialogModel> IncidentFieldValues { get; set; } = new List<IncidentFieldValueDialogModel>();
    }

    [Serializable]
    public class IncidentType
    {
        public int Id { get; set; }
    }
    [Serializable]
    public class Street
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
    [Serializable]
    public class TransportRoute
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
    [Serializable]
    public class IncidentFieldValueDialogModel
    {
        public int IncidentFieldId { get; set; }
        public string Value { get; set; }
    }
}