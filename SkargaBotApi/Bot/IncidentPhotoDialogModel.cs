﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SkargaBotApi.Bot
{
    [Serializable]
    public class IncidentPhotoDialogModel
    {
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}