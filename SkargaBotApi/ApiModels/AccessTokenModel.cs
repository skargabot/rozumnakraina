﻿
using System;

namespace SkargaBotApi.ApiModels
{
    public class AccessTokenModel
    {
        public string Token { get; set; }
        public DateTime? Expire { get; set; }
    }
}