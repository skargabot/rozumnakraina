﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataModel.Enums;

namespace SkargaBotApi.ApiModels
{
    public class FilterModel
    {
        public DateTime? Date { get; set; }
        public int[] IncidentTypesIds { get; set; }
        public IncidentStatePublic? State { get; set; }
        public int? TransportTypeId { get; set; }
        public int? RouteId { get; set; }
        public bool WithPhoto { get; set; }
        public bool SortById { get; set; }
        public bool SortAsc { get; set; }
    }
}