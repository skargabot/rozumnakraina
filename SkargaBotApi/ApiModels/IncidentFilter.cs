﻿using System;
using DataModel.Enums;

namespace SkargaBotApi.ApiModels
{
    public class IncidentFilter
    {
        public IncidentStatePublic[] State { get; set; }
        public IncidentPriority[] Priority { get; set; }
        //public IncidentType[] Type { get; set; }
        public DateTime? StarDate { get; set; }
        public DateTime? EndDate { get; set; }
        
    }
}